<center>
  <p align="center">
    <img src="https://gitlab.com/desafio-banco-inter-thales/user-manager/-/raw/main/public/logo.png" width="150"/>&nbsp;
  </p>  
  <h1 align="center">🚀 Microserviço: Gerenciador de ações</h1>
  <p align="center">
    Microserviço referente ao desafio banco inter para criação de uma mini<br /> 
    plataforma de investimentos utilizando clean arch, DDD, 12 Factor App e as<br />
    boas práticas atuais do mercado. Espero que gostem!!
  </p>
</center>
<br />

## Objetivo do microserviço

Aqui é onde a distribuição de ações dada uma ordem de compra do usuário acontece. Basicamente o usuário só precisa estar cadastrado
na base de dados, podendo ser um administrador ou não e solicitar a compra de ações passando o CPF, quantidade de ações(amountStocks) que deseja e
total da ordem de compra(order). O Sistema irá cuidar de toda distribuição.

### Como funciona a distribuição?

Ex: Temos 3 ações cadastradas no banco e enviamos essa ordem

```
{
  "userCpf": "<cpf>",
  "order": 3000,
  "amountStocks": 1
}
```

O sistema irá resgatar aleatoriamente uma ação e irá investir 3000 dinheiros
na ação.
<br/><br/>
Numa segunda rodada de investimentos o usuário conseguiu juntar mais grana e 
agora vai investir 2000 dinheiros em 2 ações:

```
{
  "userCpf": "<cpf>",
  "order": 2000,
  "amountStocks": 2
}
```

Nesse caso o sistema irá resgatar a ação que o usuário já tem e pegar mais uma
de forma aleatória. Vai investir apenas na ação que o usuário não possuia até
balancear com a primeira ação. O que não vai acontecer nessa segunda rodada
pois o usuário não investiu dinheiro suficiente.
<br/><br/>
Digamos que numa terceira rodada o usuário conseguiu juntar o mesmo valor, mas
decidiu investir em mais uma ação:

```
{
  "userCpf": "<cpf>",
  "order": 2000,
  "amountStocks": 3
}
```

Nesse caso o sistema irá dividir a grana entre a ação desbalanceada que 
foi comprada na segunda rodada e a nova ação que ainda não foi comprada.

## Ferramentas necessárias para rodar o projeto em container

- Docker
- Docker compose

## Ferramentas necessárias para o projeto localmente

- IDE de sua preferência
- Navegador de sua preferência
- JDK 17
  - Utilizei o sdkman para instalação da versão **17.0.3-zulu**
- Docker e Docker compose
  - Para rodar as dependências
- Gradle
  - Utilizei o sdkman para instalação da versão **7.5**

## Pré-execução

1. Clonar o repositório:
```sh
git clone https://gitlab.com/desafio-banco-inter-thales/stock-manager.git
```

2. Entre na pasta referente ao repositório:
```sh
cd stock-manager
```

## Execução

> Disclaimer 1 : É necessário ter ao menos um usuário cadastrado na base do 
> tipo administrador e ao menos uma empresa cadastrada por este usuário 
> para que seja possível comprar ações com esse mesmo usuário.


> Disclaimer 2: No arquivo **src/main/resources/data.sql** existem 
> inserts comentados que podem te auxiliar a não precisar passar por
> todo fluxo de criação de usuário e depois de empresas.
> Para utilizá-lo:
> 1. Opção 1(Usuário sem ações)
>    - Descomente o código da linha 11 até a linha 19;
>    - Descomente o código da linha 29;
> 2. Opção 2(Usuário com ações)
>    - Descomente o código da linha 11 até a linha 19;
>    - Descomente o código da linha 28;
>    - Descomente o código da linha 44 a 45;
>      - Caso essa última linha não seja descomentada o código irá bugar
>      - devido aos critérios de preço médio e troco que o sistema considera
>      - então com grandes poderes vem grandes responsabilidades. Escolha
>      - sabiamente.

É possível executar o projeto de duas formas:

### Apenas containers

1. Execute o comando
```shell
docker-compose -f containers/docker-compose.yml up -d --build
```

2. (user-manager) Abra o navegador em http://localhost:8080/swagger-ui/index.html
3. (company-manager) Abra o navegador em http://localhost:8081/swagger-ui/index.html
4. (stock-manager) Abra o navegador em http://localhost:8082/swagger-ui/index.html


### Aplicação rodando localmente

1. Execute o comando
```shell
docker-compose -f containers/docker-compose-deps.yml up -d --build
```
> A opção **--build** serve para não utilizarmos o cache para construção
> da imagem do user-manager, caso queira buildar uma imagem já tendo gerado
> a primeira e não quer perder tempo pode remover essa opção de --build

2. Execute a aplicação
```shell
./gradlew bootRun
```
> Também é possível executar como uma aplicação Java através do
> método main() na classe Main.java

3. (user-manager) Abra o navegador em http://localhost:8080/swagger-ui/index.html
4. (company-manager) Abra o navegador em http://localhost:8081/swagger-ui/index.html
5. (stock-manager) Abra o navegador em http://localhost:8082/swagger-ui/index.html

## Banco de dados
O banco de dados principal é um H2 e para subir localmente não precisamos de 
passos extras, a própria aplicação se encarrega de subir as tabelas descritas
no arquivo presente em ```src/main/resources/data.sql```

> É importante mencionar que pela forma que o sistema foi escrito 
> toda vez que a aplicação é derrubada nossos registros serão removidos 
> do banco

## Stop

Caso queira derrubar a aplicação

1. Execute o comando
```shell
docker-compose -f containers/docker-compose.yml down
```

## Testes

1. Execute o comando
```shell
./gradlew test
```

> Também é possível executar via IDE. Utilizando o Intellij vá até a pasta
> **src/test/java/com/desafio** clique com o botão direito e na metade 
> das opções apresentadas escolha **Run Tests in 'com.desafio''**

## Responsabilidades do microserviço

- [x] Distribuição de ativos para o usuário
- [x] Resgate dos ativos do usuário
- [x] Consumo do evento vindo do tópico ```USER_CREATED```
- [x] Consumo do evento vindo do tópico ```COMPANY_EVENT```

## Design do microserviço
<br/>
<center>
  <p align="center">
    <img src="https://gitlab.com/desafio-banco-inter-thales/stock-manager/-/raw/main/public/desafio-Stock-man-design.drawio.png"/>&nbsp;
  </p>  
</center>

### Características do microserviço

- [x] Utilizado comunicação síncrona na entrada de requisições
  - Para facilitar a criação de novos usuários
- [x] Utilizado comunicação assíncrona na saída de eventos
  - Spring cloud stream com binder do kafka
- [x] Utilizado H2 como banco de dados
- [x] Utilizado **Sleuth** para o tracing entre as aplicações
- [x] Utilizado **Zipkin** para visualização do tracing entre as aplicações
- [x] Docker para containerização
- [x] Code coverage com [sonarcloud ~ 60%](https://sonarcloud.io/project/overview?id=desafio-banco-inter-thales_stock-manager)

#### Pontos negativos do microserviço

1. Dependência muito forte do kafka sendo um single point of failure;
2. A lógica de distribuição da grana entre os ativos é bem rudimentar;
3. A aplicação não consome os eventos desde o início então se houver a criação
   de 5 usuários antes do stock-manager estar de pé, esses usuários não estarão
   cadastrados no stock-manager;

##### kafka como single point of failure

Sobre isso o autor tem em mente uma possível solução algo como uma tabela
temporária na qual sempre que houvesse falha na conexão os registros iriam
para essa tabela. Quando houvesse o retorno do servidor do kafka então o job
enviaria em lotes ou todos os registros para serem consumidos pelo 
company-manager.