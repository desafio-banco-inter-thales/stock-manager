package com.desafio.infrastructure.gateway.user;

import com.desafio.application.user.IUserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<UserJPAEntity, String> {

  Optional<IUserEntity> findByCpf(String cpf);

}
