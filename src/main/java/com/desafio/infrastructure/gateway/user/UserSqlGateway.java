package com.desafio.infrastructure.gateway.user;

import com.desafio.application.user.IUserEntity;
import com.desafio.application.user.IUserGateway;
import com.desafio.application.user.UserDto;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class UserSqlGateway implements IUserGateway {

  private final UserRepository userRepository;

  public UserSqlGateway(UserRepository userRepository) {
    this.userRepository = userRepository;
  }

  public UserJPAEntity create(UserJPAEntity user) {
    return this.userRepository.save(user);
  }

  public Optional<IUserEntity> findByCpf(String cpf) {
    return this.userRepository.findByCpf(cpf);
  }

  @Override
  public void save(UserDto user) {
    this.userRepository.save(UserJPAEntity.of(user));
  }

  public void update(UserJPAEntity update) {
    this.userRepository.save(update);
  }
}
