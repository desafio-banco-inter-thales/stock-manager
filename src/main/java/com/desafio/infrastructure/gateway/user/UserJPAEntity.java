package com.desafio.infrastructure.gateway.user;

import com.desafio.application.user.IUserEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "user")
public class UserJPAEntity implements IUserEntity {

  @Id
  private String id;

  @Column(name = "cpf", length = 11, nullable = false)
  private String cpf;

  @Column(
          name = "exchange",
          nullable = false
  )
  private BigDecimal exchange;

  @Column(
          name = "average_price",
          nullable = false
  )
  private BigDecimal averagePrice;

  private UserJPAEntity() {
  }

  private UserJPAEntity(String id, String cpf, BigDecimal exchange, BigDecimal averagePrice) {
    this.id = id;
    this.cpf = cpf;
    this.exchange = exchange;
    this.averagePrice = averagePrice;
  }

  public static UserJPAEntity of(IUserEntity iUserEntity) {
    return new UserJPAEntity(iUserEntity.id(), iUserEntity.cpf(), iUserEntity.exchange(), iUserEntity.averagePrice());
  }

  public String id() {
    return id;
  }

  public String cpf() {
    return cpf;
  }

  public BigDecimal exchange() {
    return exchange;
  }

  public BigDecimal averagePrice() {
    return averagePrice;
  }
}
