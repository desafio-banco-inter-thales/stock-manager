package com.desafio.infrastructure.gateway.company;

import com.desafio.application.company.ICompanyEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface CompanyRepository extends JpaRepository<CompanyJpaEntity, String> {

  @Query("FROM CompanyJpaEntity WHERE is_active = true")
  List<ICompanyEntity> findAllActive();

  @Query("FROM CompanyJpaEntity WHERE ticker not in (?1) and is_active = true")
  List<ICompanyEntity> findCompaniesUserDoesNotHave(List<String> tickers);

  @Query("FROM CompanyJpaEntity WHERE ticker in (?1) and is_active = true")
  List<ICompanyEntity> findCompaniesUserDoesHave(List<String> tickers);

  @Query("FROM CompanyJpaEntity WHERE ticker = ?1")
  Optional<ICompanyEntity> findByTicker(String ticker);
}