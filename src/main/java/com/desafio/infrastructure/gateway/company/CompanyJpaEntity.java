package com.desafio.infrastructure.gateway.company;

import com.desafio.application.company.CompanyDto;
import com.desafio.application.company.ICompanyEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(
        name = "company"
)
public class CompanyJpaEntity implements ICompanyEntity {

  @Id
  private String id;

  @Column(
          name = "name",
          length = 30,
          nullable = false
  )
  private String name;

  @Column(
          name = "ticker",
          length = 7,
          nullable = false
  )
  private String ticker;

  @Column(
          name = "price",
          nullable = false
  )
  private BigDecimal price;

  @Column(
          name = "is_active",
          nullable = false
  )
  private Boolean isActive;


  public CompanyJpaEntity() {
  }

  public CompanyJpaEntity(String ticker, BigDecimal price) {
    this.ticker = ticker;
    this.price = price;
  }

  public CompanyJpaEntity(String id, String name, String ticker, BigDecimal price, Boolean isActive) {
    this.id = id;
    this.name = name;
    this.ticker = ticker;
    this.price = price;
    this.isActive = isActive;
  }

  public static CompanyJpaEntity of(ICompanyEntity iCompanyEntity) {
    return new CompanyJpaEntity(iCompanyEntity.id(), iCompanyEntity.name(), iCompanyEntity.ticker(), iCompanyEntity.price(), iCompanyEntity.isActive());
  }

  public CompanyDto toAggregate() {
    return CompanyDto.of(this.id(), this.ticker(), this.name(), this.price(), this.isActive());
  }

  public String id() {
    return id;
  }

  public String name() {
    return name;
  }

  public String ticker() {
    return ticker;
  }

  public BigDecimal price() {
    return price;
  }

  public Boolean isActive() {
    return isActive;
  }
}
