package com.desafio.infrastructure.gateway.company;

import com.desafio.application.company.ICompanyEntity;
import com.desafio.application.company.ICompanyGateway;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class CompanySqlGateway implements ICompanyGateway {
  private final CompanyRepository companyRepository;

  public CompanySqlGateway(CompanyRepository companyRepository) {
    this.companyRepository = companyRepository;
  }

  public List<ICompanyEntity> findAllActive() {
    return this.companyRepository.findAllActive();
  }

  @Override
  public List<ICompanyEntity> findAllCompaniesUserAlreadyHas(List<String> tickers) {
    return this.companyRepository.findCompaniesUserDoesHave(tickers);
  }

  @Override
  public List<ICompanyEntity> findAllCompaniesUserDoesNotHave(List<String> tickers) {
    return this.companyRepository.findCompaniesUserDoesNotHave(tickers);
  }

  public List<ICompanyEntity> findCompaniesUserDoesNotHave(List<String> tickers) {
    return this.companyRepository.findCompaniesUserDoesNotHave(tickers);
  }

  @Override
  public void save(ICompanyEntity company) {
    this.companyRepository.save(CompanyJpaEntity.of(company));
  }

  @Override
  public void delete(ICompanyEntity company) {
    this.companyRepository.delete(CompanyJpaEntity.of(company));
  }

  @Override
  public Optional<ICompanyEntity> findByTicker(String ticker) {
    return this.companyRepository.findByTicker(ticker);
  }
}