package com.desafio.infrastructure.gateway.stock;

import com.desafio.application.stock.gateway.IStockEntity;
import com.desafio.infrastructure.gateway.company.CompanyJpaEntity;
import com.desafio.infrastructure.gateway.user.UserJPAEntity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.Instant;

@Entity
@Table(name = "stock")
public class StockJpaEntity implements IStockEntity {

  @Id
  private String id;

  @ManyToOne
  @JoinColumn(name="company_id")
  private CompanyJpaEntity company;

  @ManyToOne
  @JoinColumn(name="user_id")
  private UserJPAEntity user;

  @Column(
          name = "price",
          nullable = false
  )
  private BigDecimal price;

  @Column(
          name = "amount",
          nullable = false
  )
  private Integer amount;

  @Column(
          name = "created_at",
          columnDefinition = "DATETIME(6)"
  )
  private Instant createdAt;

  @Column(
          name = "updated_at",
          columnDefinition = "DATETIME(6)"
  )
  private Instant updatedAt;

  @Column(
          name = "deleted_at",
          columnDefinition = "DATETIME(6)"
  )
  private Instant deletedAt;

  public StockJpaEntity() {
  }

  public StockJpaEntity(String id, CompanyJpaEntity company, UserJPAEntity user, BigDecimal price, Integer amount, Instant createdAt, Instant updatedAt, Instant deletedAt) {
    this.id = id;
    this.company = company;
    this.user = user;
    this.price = price;
    this.amount = amount;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
    this.deletedAt = deletedAt;
  }

  public static StockJpaEntity of(String id, CompanyJpaEntity companyJpaEntity, UserJPAEntity user, BigDecimal price, Integer amount, Instant createdAt, Instant updatedAt, Instant deletedAt) {
    return new StockJpaEntity(id, companyJpaEntity, user, price, amount, createdAt, updatedAt, deletedAt);
  }

  public static StockJpaEntity of(IStockEntity iStockEntity) {
    return new StockJpaEntity(
            iStockEntity.id(),
            CompanyJpaEntity.of(iStockEntity.company()),
            UserJPAEntity.of(iStockEntity.user()),
            iStockEntity.price(),
            iStockEntity.amount(),
            iStockEntity.createdAt(),
            iStockEntity.updatedAt(),
            iStockEntity.createdAt()
    );
  }

  public String id() {
    return id;
  }

  public CompanyJpaEntity company() {
    return company;
  }

  public UserJPAEntity user() {
    return user;
  }

  public BigDecimal price() {
    return price;
  }

  public Integer amount() {
    return amount;
  }

  public Instant createdAt() {
    return createdAt;
  }

  public Instant updatedAt() {
    return updatedAt;
  }

  public Instant deletedAt() {
    return deletedAt;
  }
}
