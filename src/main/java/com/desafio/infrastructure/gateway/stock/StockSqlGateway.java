package com.desafio.infrastructure.gateway.stock;

import com.desafio.application.stock.StockDto;
import com.desafio.application.stock.gateway.IStockGateway;
import com.desafio.application.stock.gateway.StockResponse;
import com.desafio.application.stock.gateway.StockResponseFind;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class StockSqlGateway implements IStockGateway<StockDto> {

  private final StockRepository stockRepository;

  public StockSqlGateway(StockRepository stockRepository) {
    this.stockRepository = stockRepository;
  }

  public List<StockResponse> findAllTickersByUser(String id) {
    return this.stockRepository.findAllTickersByUser(id);
  }

  @Override
  public List<StockResponseFind> findAllStocksToUser(String cpf) {
    return this.stockRepository.findAllStocksToUser(cpf);
  }

  @Override
  public void deleteAllByCompanyTicker(String ticker) {
    List<StockJpaEntity> stockJpaEntities = this.stockRepository.findAllStocksByCompanyTicker(ticker);
    if(stockJpaEntities.isEmpty()) return;

    for (StockJpaEntity stock : stockJpaEntities) {
      this.stockRepository.delete(stock);
    }
  }

  @Override
  public void createAll(List<StockDto> stocks) {
    List<StockJpaEntity> entities = stocks.stream().map(StockJpaEntity::of).toList();
    this.stockRepository.saveAll(entities);
  }

}
