package com.desafio.infrastructure.gateway.stock;

import com.desafio.application.stock.gateway.StockResponse;
import com.desafio.application.stock.gateway.StockResponseFind;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface StockRepository extends JpaRepository<StockJpaEntity, String> {

  @Query("SELECT s.company.ticker as ticker, sum(s.amount) as amount, s.company as company, Avg(s.price) as price, s.user.exchange as exchange FROM StockJpaEntity s INNER JOIN s.user u WHERE u.id = ?1 GROUP BY s.company.ticker")
  List<StockResponse> findAllTickersByUser(String id);

  @Query("SELECT s.company.ticker as ticker, s.price as price, s.company.isActive as active, s.amount as amount, s.user.exchange as exchange, s.user.averagePrice as averagePrice FROM StockJpaEntity s INNER JOIN s.user u WHERE u.cpf = ?1")
  List<StockResponseFind> findAllStocksToUser(String cpf);

  @Query("FROM StockJpaEntity s INNER JOIN s.company c WHERE c.ticker = ?1")
  List<StockJpaEntity> findAllStocksByCompanyTicker(String ticker);
}
