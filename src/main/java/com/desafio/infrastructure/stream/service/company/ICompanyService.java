package com.desafio.infrastructure.stream.service.company;

import com.desafio.infrastructure.stream.events.CompanyEvent;

public interface ICompanyService {
  void execute(CompanyEvent companyEvent);
}
