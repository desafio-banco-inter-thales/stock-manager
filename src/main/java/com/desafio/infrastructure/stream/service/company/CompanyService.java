package com.desafio.infrastructure.stream.service.company;

import com.desafio.infrastructure.stream.events.CompanyEvent;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class CompanyService {

  SaveCompanyService saveCompanyService;

  DeleteCompanyService deleteCompanyService;

  private CompanyService(SaveCompanyService saveCompanyService, DeleteCompanyService deleteCompanyService) {
    this.saveCompanyService = saveCompanyService;
    this.deleteCompanyService = deleteCompanyService;
  }

  public static CompanyService of(SaveCompanyService saveCompanyService, DeleteCompanyService deleteCompanyService) {
    return new CompanyService(saveCompanyService, deleteCompanyService);
  }

  public void execute(CompanyEvent event) {
    Map<String, ICompanyService> strategies = Map.of(
            "save", saveCompanyService,
            "delete", deleteCompanyService
    );

    strategies.get(event.type()).execute(event);
  }
}
