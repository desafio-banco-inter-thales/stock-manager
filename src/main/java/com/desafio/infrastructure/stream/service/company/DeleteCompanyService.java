package com.desafio.infrastructure.stream.service.company;

import com.desafio.application.company.CompanyDto;
import com.desafio.application.company.ICompanyEntity;
import com.desafio.application.company.delete.DeleteCompanyCommand;
import com.desafio.application.company.delete.DeleteCompanyUseCase;
import com.desafio.application.company.find.all.FindAllCompaniesUserAlreadyHasUseCase;
import com.desafio.application.company.find.output.FindCompanyByTickerOutput;
import com.desafio.application.company.find.ticker.FindCompanyByTickerCommand;
import com.desafio.application.company.find.ticker.FindCompanyByTickerUseCase;
import com.desafio.application.stock.delete.DeleteAllStocksByCompanyTickerCommand;
import com.desafio.application.stock.delete.DeleteAllStocksByCompanyTickerUseCase;
import com.desafio.application.stock.find.by.FindStocksByUserUseCase;
import com.desafio.application.stock.find.by.FindStocksByUserUseCaseCommand;
import com.desafio.application.stock.mapper.FindAllTickersByUserMapper;
import com.desafio.application.user.UserDto;
import com.desafio.application.user.create.CreateUserUseCase;
import com.desafio.application.user.find.FindUserUseCase;
import com.desafio.application.user.find.FindUserUseCaseCommand;
import com.desafio.application.user.update.UpdateUserUseCase;
import com.desafio.application.user.update.UpdateUserUseCaseCommand;
import com.desafio.infrastructure.stream.events.CompanyEvent;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

import static java.math.BigDecimal.ONE;
import static java.math.BigDecimal.ZERO;

@Service
public class DeleteCompanyService implements ICompanyService {

  private final FindCompanyByTickerUseCase findCompanyByTickerUseCase;
  private final FindUserUseCase findUserUseCase;
  private final FindStocksByUserUseCase findStocksByUserUseCase;
  private final UpdateUserUseCase updateUserUseCase;
  private final DeleteCompanyUseCase deleteCompanyUseCase;
  private final DeleteAllStocksByCompanyTickerUseCase deleteAllStocksByCompanyTickerUseCase;

  private DeleteCompanyService(
          FindCompanyByTickerUseCase findCompanyByTickerUseCase,
          FindUserUseCase findUserUseCase,
          FindStocksByUserUseCase findStocksByUserUseCase,
          UpdateUserUseCase updateUserUseCase,
          DeleteCompanyUseCase deleteCompanyUseCase,
          DeleteAllStocksByCompanyTickerUseCase deleteAllStocksByCompanyTickerUseCase) {
    this.findCompanyByTickerUseCase = findCompanyByTickerUseCase;
    this.findUserUseCase = findUserUseCase;
    this.findStocksByUserUseCase = findStocksByUserUseCase;
    this.updateUserUseCase = updateUserUseCase;
    this.deleteCompanyUseCase = deleteCompanyUseCase;
    this.deleteAllStocksByCompanyTickerUseCase = deleteAllStocksByCompanyTickerUseCase;
  }

  public static DeleteCompanyService of(
          FindCompanyByTickerUseCase findCompanyByTickerUseCase,
          DeleteCompanyUseCase deleteCompanyUseCase,
          FindUserUseCase findUserUseCase,
          FindStocksByUserUseCase findStocksByUserUseCase,
          UpdateUserUseCase updateUserUseCase,
          DeleteAllStocksByCompanyTickerUseCase deleteAllStocksByCompanyTickerUseCase) {
    return new DeleteCompanyService(findCompanyByTickerUseCase, findUserUseCase, findStocksByUserUseCase, updateUserUseCase, deleteCompanyUseCase, deleteAllStocksByCompanyTickerUseCase);
  }

  @Override
  public void execute(CompanyEvent companyEvent) {
    FindCompanyByTickerOutput output = findCompanyByTickerUseCase.execute(FindCompanyByTickerCommand.of(companyEvent.ticker()));


    if(output.notificationErrors() != null)
      return;

    ICompanyEntity entity = output.company();

    ICompanyEntity iCompanyEntity = CompanyDto.of(entity.id(), companyEvent.name(), companyEvent.ticker(), companyEvent.price(), companyEvent.isActive());

    deleteAllStocksByCompanyTickerUseCase.execute(DeleteAllStocksByCompanyTickerCommand.of(iCompanyEntity.ticker()));
    deleteCompanyUseCase.execute(DeleteCompanyCommand.of(iCompanyEntity.ticker()));
    final var outputUser = findUserUseCase.execute(FindUserUseCaseCommand.of(companyEvent.userCpf()));
    final var user = outputUser.userDto();
    final var outputCompaniesOfUser = findStocksByUserUseCase.execute(FindStocksByUserUseCaseCommand.of(user));
    BigDecimal averagePrice = this.getAveragePrice(outputCompaniesOfUser.findAllTickersByUserMapperList());
    updateUserUseCase.execute(UpdateUserUseCaseCommand.of(user.id(), user.cpf(), user.exchange(), averagePrice));
  }

  public BigDecimal getAveragePrice(List<FindAllTickersByUserMapper> mappers) {
    BigDecimal total = ZERO;
    BigDecimal amount = ZERO;
    for (FindAllTickersByUserMapper mapper : mappers) {
      if(mapper.total() != null) {
        total = total.add(mapper.total());
        amount = amount.add(ONE);
      }
    }

    if(amount.equals(ZERO))
      return ZERO;

    return total.divide(amount, 2, RoundingMode.HALF_UP);
  }

}
