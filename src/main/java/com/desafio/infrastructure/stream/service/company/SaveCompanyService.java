package com.desafio.infrastructure.stream.service.company;


import com.desafio.application.company.CompanyDto;
import com.desafio.application.company.ICompanyEntity;
import com.desafio.application.company.create.CreateCompanyCommand;
import com.desafio.application.company.create.CreateCompanyUseCase;
import com.desafio.application.company.find.output.FindCompanyByTickerOutput;
import com.desafio.application.company.find.ticker.FindCompanyByTickerCommand;
import com.desafio.application.company.find.ticker.FindCompanyByTickerUseCase;
import com.desafio.infrastructure.stream.events.CompanyEvent;
import com.desafio.shared.log.ILog;
import com.desafio.shared.log.Log;
import org.springframework.stereotype.Service;

import static com.desafio.shared.utils.SuccessConstants.COMPANY_SAVED_ON_GATEWAY;

@Service
public class SaveCompanyService implements ICompanyService {

  private ILog log = new Log(SaveCompanyService.class);

  private final FindCompanyByTickerUseCase findCompanyByTickerUseCase;

  private final CreateCompanyUseCase createCompanyUseCase;

  private SaveCompanyService(
          FindCompanyByTickerUseCase findCompanyByTickerUseCase,
          CreateCompanyUseCase createCompanyUseCase) {
    this.findCompanyByTickerUseCase = findCompanyByTickerUseCase;
    this.createCompanyUseCase = createCompanyUseCase;
  }

  public static SaveCompanyService of(
          FindCompanyByTickerUseCase findCompanyByTickerUseCase,
          CreateCompanyUseCase createCompanyUseCase) {
    return new SaveCompanyService(findCompanyByTickerUseCase, createCompanyUseCase);
  }

  @Override
  public void execute(CompanyEvent companyEvent) {
    FindCompanyByTickerOutput output = this.findCompanyByTickerUseCase.execute(FindCompanyByTickerCommand.of(companyEvent.ticker()));

    ICompanyEntity iCompanyEntity;
    if(output.notificationErrors() != null)
      iCompanyEntity = CompanyDto.of(companyEvent.id(), companyEvent.name(), companyEvent.ticker(), companyEvent.price(), companyEvent.isActive());
    else
      iCompanyEntity = CompanyDto.of(output.company().id(), companyEvent.name(), companyEvent.ticker(), companyEvent.price(), companyEvent.isActive());

    this.createCompanyUseCase.execute(CreateCompanyCommand.of(iCompanyEntity));
  }

}
