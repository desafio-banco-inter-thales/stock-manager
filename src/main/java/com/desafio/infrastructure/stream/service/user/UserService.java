package com.desafio.infrastructure.stream.service.user;

import com.desafio.application.user.create.CreateUserCommand;
import com.desafio.application.user.create.CreateUserUseCase;
import com.desafio.infrastructure.stream.events.UserEvent;
import org.springframework.stereotype.Service;

@Service
public class UserService {

  private final CreateUserUseCase useCase;

  private UserService(CreateUserUseCase useCase) {
    this.useCase = useCase;
  }

  public static UserService with(CreateUserUseCase useCase) {
    return new UserService(useCase);
  }

  public void execute(UserEvent event) {
    final var command = CreateUserCommand.of(event.cpf());
    this.useCase.execute(command);
  }
}
