package com.desafio.infrastructure.api.request;

import java.math.BigDecimal;

public record CreateStockRequest(String userCpf, BigDecimal order, int amountStocks) {

  public static CreateStockRequest of(String userCpf, BigDecimal order, int amountStocks) {
    return new CreateStockRequest(userCpf, order, amountStocks);
  }
}