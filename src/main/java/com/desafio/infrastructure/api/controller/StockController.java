package com.desafio.infrastructure.api.controller;

import com.desafio.application.stock.find.to.FindStocksToUserUseCaseOutput;
import com.desafio.infrastructure.api.StockApi;
import com.desafio.infrastructure.api.request.CreateStockRequest;
import com.desafio.infrastructure.api.service.CreateStockService;
import com.desafio.infrastructure.api.service.FindUserStocksService;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;
import java.util.Map;

@RestController
public class StockController implements StockApi {

  private final CreateStockService createStockService;
  private final FindUserStocksService findUserStocksService;

  public StockController(
          CreateStockService createStockService,
          FindUserStocksService findUserStocksService
  ) {
    this.createStockService = createStockService;
    this.findUserStocksService = findUserStocksService;
  }

  @Override
  public ResponseEntity<?> createStocks(CreateStockRequest input) throws JsonProcessingException {
    final var userCpf = input.userCpf();
    final var amount = input.amountStocks();
    final var order = input.order();

    final var output = createStockService.execute(userCpf, amount, order);

    return output.message() == null ?
            ResponseEntity.created(URI.create("/"+userCpf)).build()
            :
            ResponseEntity.unprocessableEntity().body(Map.of("message", output.message()));
  }

  @Override
  public ResponseEntity<?> getStocks(String cpf) {
    FindStocksToUserUseCaseOutput response = this.findUserStocksService.execute(cpf);
    return response.notification() != null ?
            ResponseEntity.ok().body(response.notification())
            :
            ResponseEntity.ok().body(response);
  }

}
