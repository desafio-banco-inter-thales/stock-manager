package com.desafio.infrastructure.api.service;

import com.desafio.application.company.CompanyDto;
import com.desafio.application.stock.GetAveragePriceUseCase;
import com.desafio.application.stock.StockDto;
import com.desafio.application.stock.buy.BuyStocksOfUserUseCase;
import com.desafio.application.stock.buy.BuyUserStocksUseCaseCommand;
import com.desafio.application.stock.buy.BuyUserStocksUseCaseOutput;
import com.desafio.application.stock.choose.ChooseStocksToBuyUseCase;
import com.desafio.application.stock.choose.ChooseStocksToBuyUseCaseCommand;
import com.desafio.application.stock.choose.ChooseStocksToBuyUseCaseOutput;
import com.desafio.application.stock.create.CreateStocksForUserUseCaseCommand;
import com.desafio.application.stock.create.CreateStocksOfUserUseCase;
import com.desafio.application.stock.find.by.FindStocksByUserUseCase;
import com.desafio.application.stock.find.by.FindStocksByUserUseCaseCommand;
import com.desafio.application.stock.find.by.FindStocksByUserUseCaseOutput;
import com.desafio.application.stock.mapper.FindAllTickersByUserMapper;
import com.desafio.application.user.find.FindUserUseCase;
import com.desafio.application.user.find.FindUserUseCaseCommand;
import com.desafio.application.user.find.FindUserUseCaseOutput;
import com.desafio.application.user.update.UpdateUserUseCase;
import com.desafio.application.user.update.UpdateUserUseCaseCommand;
import com.desafio.infrastructure.api.response.CreateStockResponse;
import com.desafio.shared.log.ILog;
import com.desafio.shared.log.Log;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

import static com.desafio.shared.utils.ErrorConstants.*;
import static java.math.BigDecimal.ONE;
import static java.math.BigDecimal.ZERO;

@Service
public class CreateStockService {

  private ILog log = new Log(CreateStockService.class);

  private final FindUserUseCase findUserUseCase;
  private final UpdateUserUseCase updateUserUseCase;
  private final FindStocksByUserUseCase findStocksByUserUseCase;
  private final ChooseStocksToBuyUseCase chooseStocksToBuyUseCase;
  private final BuyStocksOfUserUseCase buyUserStocksUseCase;
  private final CreateStocksOfUserUseCase createStocksForUserUseCase;
  private final GetAveragePriceUseCase getAveragePriceUseCase;

  public CreateStockService(FindUserUseCase findUserUseCase, UpdateUserUseCase updateUserUseCase, FindStocksByUserUseCase findStocksByUserUseCase, ChooseStocksToBuyUseCase chooseStocksToBuyUseCase, BuyStocksOfUserUseCase buyUserStocksUseCase, CreateStocksOfUserUseCase createStocksForUserUseCase, GetAveragePriceUseCase getAveragePriceUseCase) {
    this.findUserUseCase = findUserUseCase;
    this.updateUserUseCase = updateUserUseCase;
    this.findStocksByUserUseCase = findStocksByUserUseCase;
    this.chooseStocksToBuyUseCase = chooseStocksToBuyUseCase;
    this.buyUserStocksUseCase = buyUserStocksUseCase;
    this.createStocksForUserUseCase = createStocksForUserUseCase;
    this.getAveragePriceUseCase = getAveragePriceUseCase;
  }

  public CreateStockResponse execute(String userCpf, int amountOfStocks, BigDecimal order) {
    if(amountOfStocks == 0) {
      log.info(CANNOT_BUY_ZERO_STOCKS);
      return CreateStockResponse.of(CANNOT_BUY_ZERO_STOCKS);
    }


    FindUserUseCaseOutput findUserUseCaseOutput = findUserUseCase.execute(FindUserUseCaseCommand.of(userCpf));
    if(findUserUseCaseOutput.notification() != null) {
      log.info(USER_NOT_FOUND);
      return CreateStockResponse.of(USER_NOT_FOUND);
    }

    final var userDto = findUserUseCaseOutput.userDto();
    final var exchange = userDto.exchange();
    final var averagePrice = userDto.averagePrice();
    order = order.add(exchange);

    FindStocksByUserUseCaseOutput findStocksByUserUseCaseOutput =
            findStocksByUserUseCase.execute(FindStocksByUserUseCaseCommand.of(userDto));
    ChooseStocksToBuyUseCaseOutput chooseStocksToBuyUseCaseOutput =
            chooseStocksToBuyUseCase.execute(ChooseStocksToBuyUseCaseCommand.of(findStocksByUserUseCaseOutput.findAllTickersByUserMapperList(), userDto, amountOfStocks));

    if(chooseStocksToBuyUseCaseOutput.stockDtoList().size() == 0) {
      log.info(THERE_ARE_NO_ACTIVE_STOCKS);
      return CreateStockResponse.of(THERE_ARE_NO_ACTIVE_STOCKS);
    }

    BuyUserStocksUseCaseOutput buyUserStocksUseCaseOutput = buyUserStocksUseCase.execute(BuyUserStocksUseCaseCommand.of(order, chooseStocksToBuyUseCaseOutput.stockDtoList(), averagePrice));

    createStocksForUserUseCase.execute(CreateStocksForUserUseCaseCommand.of(buyUserStocksUseCaseOutput.stocks()));

    final var output = findStocksByUserUseCase.execute(FindStocksByUserUseCaseCommand.of(userDto));

    updateUserUseCase.execute(
            UpdateUserUseCaseCommand.of(
                    userDto.id(),
                    userDto.cpf(),
                    buyUserStocksUseCaseOutput.exchange(),
                    this.getAveragePrice(output.findAllTickersByUserMapperList())
            )
    );

    return CreateStockResponse.of(buyUserStocksUseCaseOutput);
  }

  public BigDecimal getAveragePrice(List<FindAllTickersByUserMapper> mappers) {
    BigDecimal total = ZERO;
    BigDecimal amount = ZERO;
    for (FindAllTickersByUserMapper mapper : mappers) {
      if(mapper.total() != null) {
        total = total.add(mapper.total());
        amount = amount.add(ONE);
      }
    }

    if(amount.equals(ZERO))
      return ZERO;

    return total.divide(amount, 2, RoundingMode.HALF_UP);
  }
}
