package com.desafio.infrastructure.api.service;

import com.desafio.application.stock.find.to.FindStocksToUserUseCase;
import com.desafio.application.stock.find.to.FindStocksToUserUseCaseCommand;
import com.desafio.application.stock.find.to.FindStocksToUserUseCaseOutput;
import com.desafio.application.user.find.FindUserUseCase;
import com.desafio.application.user.find.FindUserUseCaseCommand;
import com.desafio.application.user.find.FindUserUseCaseOutput;
import com.desafio.domain.user.cpf.Cpf;
import com.desafio.domain.user.cpf.CpfValidator;
import com.desafio.shared.notification.INotification;
import com.desafio.shared.notification.Notification;
import org.springframework.stereotype.Service;

import static com.desafio.shared.utils.Constants.USER_STR;
import static com.desafio.shared.utils.ErrorConstants.USER_NOT_FOUND;

@Service
public class FindUserStocksService {

  private final FindUserUseCase findUserUseCase;
  private final FindStocksToUserUseCase findStocksToUserUseCase;

  public FindUserStocksService(FindUserUseCase findUserUseCase, FindStocksToUserUseCase findStocksToUserUseCase) {
    this.findUserUseCase = findUserUseCase;
    this.findStocksToUserUseCase = findStocksToUserUseCase;
  }

  public FindStocksToUserUseCaseOutput execute(String cpfStr) {
    final var cpf = Cpf.of(cpfStr);
    INotification notification = new Notification();
    new CpfValidator().validate(cpf, notification);
    if(notification.hasErrors())
      return FindStocksToUserUseCaseOutput.of(notification);

    FindUserUseCaseOutput output = findUserUseCase.execute(FindUserUseCaseCommand.of(cpfStr));
    if(output == null)
      return FindStocksToUserUseCaseOutput.of(USER_NOT_FOUND.replace("{}", cpfStr), USER_STR);

    return findStocksToUserUseCase.execute(FindStocksToUserUseCaseCommand.of(cpfStr));
  }
}
