package com.desafio.infrastructure.api;

import com.desafio.infrastructure.api.request.CreateStockRequest;
import com.fasterxml.jackson.core.JsonProcessingException;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequestMapping(value = "broker")
@Tag(name = "Broker")
public interface StockApi {

  @PostMapping(
          consumes = MediaType.APPLICATION_JSON_VALUE,
          produces = MediaType.APPLICATION_JSON_VALUE
  )
  @Operation(summary = "Create a new order of stocks")
  @ApiResponses(value = {
          @ApiResponse(responseCode = "201", description = "Created successfully"),
          @ApiResponse(responseCode = "422", description = "A validation error was thrown"),
          @ApiResponse(responseCode = "500", description = "An internal server error was thrown"),
  })
  ResponseEntity<?> createStocks(@RequestBody CreateStockRequest input) throws JsonProcessingException;

  @GetMapping(
          value = "{cpf}",
          produces = MediaType.APPLICATION_JSON_VALUE
  )
  @Operation(summary = "Get list of user stocks")
  @ApiResponses(value = {
          @ApiResponse(responseCode = "200", description = "Stocks retrieved successfully"),
          @ApiResponse(responseCode = "404", description = "User was not found"),
          @ApiResponse(responseCode = "500", description = "An internal server error was thrown"),
  })
  ResponseEntity<?> getStocks(@PathVariable(name = "cpf") String cpf);
}
