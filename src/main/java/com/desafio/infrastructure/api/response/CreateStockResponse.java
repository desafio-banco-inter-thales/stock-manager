package com.desafio.infrastructure.api.response;

import com.desafio.application.stock.StockDto;
import com.desafio.application.stock.buy.BuyUserStocksUseCaseOutput;

import java.math.BigDecimal;
import java.util.List;

public record CreateStockResponse(List<StockDto> stocks, BigDecimal exchange, BigDecimal averagePrice, String message) {

  public static CreateStockResponse of(String message) {
    return new CreateStockResponse(null, null, null, message);
  }

  public static CreateStockResponse of(BuyUserStocksUseCaseOutput output) {
    return new CreateStockResponse(output.stocks(), output.exchange(), output.averagePrice(), null);
  }
}
