package com.desafio.infrastructure.configuration;

import com.desafio.application.company.create.CreateCompanyUseCase;
import com.desafio.application.company.delete.DeleteCompanyUseCase;
import com.desafio.application.company.find.all.FindAllActiveCompaniesUseCase;
import com.desafio.application.company.find.all.FindAllCompaniesUserAlreadyHasUseCase;
import com.desafio.application.company.find.all.FindAllCompaniesUserDoesNotHaveUseCase;
import com.desafio.application.company.find.ticker.FindCompanyByTickerUseCase;
import com.desafio.application.stock.buy.BuyStocksOfUserUseCase;
import com.desafio.application.stock.choose.ChooseStocksToBuyUseCase;
import com.desafio.application.stock.create.CreateStocksOfUserUseCase;
import com.desafio.application.stock.delete.DeleteAllStocksByCompanyTickerUseCase;
import com.desafio.application.stock.find.by.FindStocksByUserUseCase;
import com.desafio.application.stock.find.to.FindStocksToUserUseCase;
import com.desafio.application.stock.GetAveragePriceUseCase;
import com.desafio.application.stock.SortListOfStocksUseCase;
import com.desafio.application.user.create.CreateUserUseCase;
import com.desafio.application.user.find.FindUserUseCase;
import com.desafio.application.user.update.UpdateUserUseCase;
import com.desafio.infrastructure.gateway.company.CompanySqlGateway;
import com.desafio.infrastructure.gateway.stock.StockSqlGateway;
import com.desafio.infrastructure.gateway.user.UserSqlGateway;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class UseCasesConfig {

  private final StockSqlGateway stockSqlGateway;
  private final UserSqlGateway userSqlGateway;
  private final CompanySqlGateway companySqlGateway;

  public UseCasesConfig(final StockSqlGateway stockSqlGateway, UserSqlGateway userSqlGateway, CompanySqlGateway companySqlGateway) {
    this.stockSqlGateway = stockSqlGateway;
    this.userSqlGateway = userSqlGateway;
    this.companySqlGateway = companySqlGateway;
  }

  @Bean
  FindUserUseCase findUserUseCase() {
    return FindUserUseCase.of(userSqlGateway);
  }

  @Bean
  UpdateUserUseCase updateUserUseCase() {
    return UpdateUserUseCase.of(userSqlGateway);
  }

  @Bean
  CreateUserUseCase createUserUseCase() {
    return CreateUserUseCase.of(userSqlGateway);
  }

  @Bean
  CreateStocksOfUserUseCase createStocksOfUserUseCase() {
    return CreateStocksOfUserUseCase.of(stockSqlGateway);
  }

  @Bean
  FindStocksByUserUseCase findStocksByUserUseCase() {
    return FindStocksByUserUseCase.of(stockSqlGateway);
  }

  @Bean
  FindAllActiveCompaniesUseCase findAllActiveCompaniesUseCase() {
    return FindAllActiveCompaniesUseCase.of(companySqlGateway);
  }

  @Bean
  FindAllCompaniesUserDoesNotHaveUseCase findAllCompaniesUserDoesNotHaveUseCase() {
    return FindAllCompaniesUserDoesNotHaveUseCase.of(companySqlGateway);
  }

  @Bean
  FindAllCompaniesUserAlreadyHasUseCase findAllCompaniesUserAlreadyHasUseCase() {
    return FindAllCompaniesUserAlreadyHasUseCase.of(companySqlGateway);
  }
  @Bean
  ChooseStocksToBuyUseCase chooseStocksToBuyUseCase() {
    return ChooseStocksToBuyUseCase.of(
            findAllCompaniesUserAlreadyHasUseCase(),
            findAllCompaniesUserDoesNotHaveUseCase(),
            findAllActiveCompaniesUseCase());
  }

  @Bean
  GetAveragePriceUseCase getAveragePriceUseCase() {
    return GetAveragePriceUseCase.of();
  }

  @Bean
  SortListOfStocksUseCase sortListOfStocksUseCase() {
    return SortListOfStocksUseCase.of(getAveragePriceUseCase());
  }

  @Bean
  BuyStocksOfUserUseCase buyStocksOfUserUseCase() {
    return BuyStocksOfUserUseCase.of(getAveragePriceUseCase(), sortListOfStocksUseCase());
  }

  @Bean
  FindStocksToUserUseCase findStocksToUserUseCase() {
    return FindStocksToUserUseCase.of(stockSqlGateway);
  }

  @Bean
  FindCompanyByTickerUseCase findCompanyByTickerUseCase() {
    return FindCompanyByTickerUseCase.of(this.companySqlGateway);
  }

  @Bean
  CreateCompanyUseCase createCompanyUseCase() {
    return CreateCompanyUseCase.of(this.companySqlGateway, findCompanyByTickerUseCase());
  }

  @Bean
  DeleteCompanyUseCase deleteCompanyUseCase() {
    return DeleteCompanyUseCase.of(companySqlGateway, findCompanyByTickerUseCase());
  }

  @Bean
  DeleteAllStocksByCompanyTickerUseCase deleteAllStocksByCompanyTickerUseCase() {
    return DeleteAllStocksByCompanyTickerUseCase.of(stockSqlGateway);
  }
}
