package com.desafio.application.company.find.output;

import com.desafio.application.company.ICompanyEntity;

import java.util.List;

public record CommonFindCompaniesUseCaseOutput(List<ICompanyEntity> companies) {

  public static CommonFindCompaniesUseCaseOutput of(List<ICompanyEntity> companies) {
    return new CommonFindCompaniesUseCaseOutput(companies);
  }

}
