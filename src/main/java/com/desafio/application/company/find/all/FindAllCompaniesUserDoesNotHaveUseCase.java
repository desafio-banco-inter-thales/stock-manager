package com.desafio.application.company.find.all;

import com.desafio.application.company.ICompanyGateway;
import com.desafio.application.company.find.output.CommonFindCompaniesUseCaseOutput;
import com.desafio.shared.log.ILog;
import com.desafio.shared.log.Log;

import java.util.List;

import static com.desafio.shared.utils.ActionConstants.GETTING_ALL_COMPANIES_USER_ALREADY_HAS;
import static com.desafio.shared.utils.ActionConstants.GETTING_ALL_COMPANIES_USER_DOES_NOT_HAVE;
import static com.desafio.shared.utils.SuccessConstants.GOT_X_COMPANIES;

public class FindAllCompaniesUserDoesNotHaveUseCase {

  private final ILog log = new Log(FindAllCompaniesUserDoesNotHaveUseCase.class);

  private final ICompanyGateway companyGateway;

  private FindAllCompaniesUserDoesNotHaveUseCase(ICompanyGateway companyGateway) {
    this.companyGateway = companyGateway;
  }

  public static FindAllCompaniesUserDoesNotHaveUseCase of(ICompanyGateway companyGateway) {
    return new FindAllCompaniesUserDoesNotHaveUseCase(companyGateway);
  }

  public CommonFindCompaniesUseCaseOutput execute(List<String> tickers) {
    log.info(GETTING_ALL_COMPANIES_USER_DOES_NOT_HAVE);
    final var output = CommonFindCompaniesUseCaseOutput.of(this.companyGateway.findAllCompaniesUserDoesNotHave(tickers));
    log.info(GOT_X_COMPANIES, output.companies().size());
    return output;
  }

}
