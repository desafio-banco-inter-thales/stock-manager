package com.desafio.application.company.find.output;

import com.desafio.application.company.ICompanyEntity;
import com.desafio.shared.notification.INotification;
import com.desafio.shared.notification.Notification;
import com.desafio.shared.notification.NotificationErrorProps;

import static com.desafio.shared.utils.Constants.COMPANY_STR;

public record FindCompanyByTickerOutput(
        ICompanyEntity company,
        INotification notificationErrors) {

  public static FindCompanyByTickerOutput of(String message) {
    INotification notification = new Notification();
    notification.append(new NotificationErrorProps(message, COMPANY_STR));
    return new FindCompanyByTickerOutput(null, notification);
  }

  public static FindCompanyByTickerOutput of(ICompanyEntity company) {
    return new FindCompanyByTickerOutput(company, null);
  }

}
