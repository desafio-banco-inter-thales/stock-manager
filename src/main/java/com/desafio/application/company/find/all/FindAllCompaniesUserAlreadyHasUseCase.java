package com.desafio.application.company.find.all;

import com.desafio.application.company.ICompanyGateway;
import com.desafio.application.company.find.output.CommonFindCompaniesUseCaseOutput;
import com.desafio.shared.log.ILog;
import com.desafio.shared.log.Log;

import java.util.List;

import static com.desafio.shared.utils.ActionConstants.GETTING_ALL_COMPANIES_USER_ALREADY_HAS;
import static com.desafio.shared.utils.SuccessConstants.GOT_X_COMPANIES;

public class FindAllCompaniesUserAlreadyHasUseCase {

  private final ILog log = new Log(FindAllCompaniesUserAlreadyHasUseCase.class);

  private final ICompanyGateway companyGateway;

  private FindAllCompaniesUserAlreadyHasUseCase(ICompanyGateway companyGateway) {
    this.companyGateway = companyGateway;
  }

  public static FindAllCompaniesUserAlreadyHasUseCase of(ICompanyGateway companyGateway) {
    return new FindAllCompaniesUserAlreadyHasUseCase(companyGateway);
  }

  public CommonFindCompaniesUseCaseOutput execute(List<String> tickers) {
    log.info(GETTING_ALL_COMPANIES_USER_ALREADY_HAS);
    final var output = CommonFindCompaniesUseCaseOutput.of(this.companyGateway.findAllCompaniesUserAlreadyHas(tickers));
    log.info(GOT_X_COMPANIES, String.valueOf(output.companies().size()));
    return output;
  }

}
