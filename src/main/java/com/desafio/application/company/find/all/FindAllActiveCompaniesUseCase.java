package com.desafio.application.company.find.all;

import com.desafio.application.company.ICompanyGateway;
import com.desafio.application.company.find.output.CommonFindCompaniesUseCaseOutput;
import com.desafio.shared.log.ILog;
import com.desafio.shared.log.Log;

import static com.desafio.shared.utils.ActionConstants.GETTING_ALL_ACTIVE_COMPANIES;
import static com.desafio.shared.utils.SuccessConstants.GOT_X_COMPANIES;

public class FindAllActiveCompaniesUseCase {

  private final ICompanyGateway companyGateway;

  private final ILog log = new Log(FindAllActiveCompaniesUseCase.class);

  private FindAllActiveCompaniesUseCase(ICompanyGateway companyGateway) {
    this.companyGateway = companyGateway;
  }

  public static FindAllActiveCompaniesUseCase of(ICompanyGateway companyGateway) {
    return new FindAllActiveCompaniesUseCase(companyGateway);
  }

  public CommonFindCompaniesUseCaseOutput execute() {
    log.info(GETTING_ALL_ACTIVE_COMPANIES);
    final var output = CommonFindCompaniesUseCaseOutput.of(this.companyGateway.findAllActive());
    log.info(GOT_X_COMPANIES, String.valueOf(output.companies().size()));
    return output;
  }

}
