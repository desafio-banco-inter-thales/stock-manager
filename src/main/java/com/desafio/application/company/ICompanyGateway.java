package com.desafio.application.company;

import java.util.List;
import java.util.Optional;

public interface ICompanyGateway {

  List<ICompanyEntity> findAllActive();
  List<ICompanyEntity> findAllCompaniesUserAlreadyHas(List<String> tickers);
  List<ICompanyEntity> findAllCompaniesUserDoesNotHave(List<String> tickers);
  void save(ICompanyEntity company);
  void delete(ICompanyEntity company);
  Optional<ICompanyEntity> findByTicker(String ticker);

}
