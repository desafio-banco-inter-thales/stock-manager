package com.desafio.application.company;

import java.math.BigDecimal;

public interface ICompanyEntity {

  String name();
  String id();
  String ticker();
  BigDecimal price();
  Boolean isActive();
}
