package com.desafio.application.company.create;

import com.desafio.application.company.ICompanyEntity;

import java.math.BigDecimal;

public record CreateCompanyCommand(String id, String name, String ticker, BigDecimal price, boolean isActive) {

  public static CreateCompanyCommand of(String name, String ticker, BigDecimal price, boolean isActive) {
    return new CreateCompanyCommand(null, name, ticker, price, isActive);
  }

  public static CreateCompanyCommand of(ICompanyEntity entity) {
    return new CreateCompanyCommand(entity.id(), entity.name(), entity.ticker(), entity.price(), entity.isActive());
  }

}
