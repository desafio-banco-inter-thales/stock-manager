package com.desafio.application.company.delete;

public record DeleteCompanyCommand(String ticker) {

  public static DeleteCompanyCommand of(String ticker) {
    return new DeleteCompanyCommand(ticker);
  }

}
