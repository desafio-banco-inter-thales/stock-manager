package com.desafio.application.user.find;

import com.desafio.application.stock.find.to.FindStocksToUserUseCaseOutput;
import com.desafio.application.user.UserDto;
import com.desafio.shared.notification.INotification;
import com.desafio.shared.notification.Notification;
import com.desafio.shared.notification.NotificationErrorProps;

import java.math.BigDecimal;

import static com.desafio.shared.utils.Constants.STOCK_STR;
import static com.desafio.shared.utils.Constants.USER_STR;

public record FindUserUseCaseOutput(UserDto userDto, INotification notification) {

  public static FindUserUseCaseOutput of(String id, String cpf, BigDecimal exchange, BigDecimal averagePrice) {
    return new FindUserUseCaseOutput(UserDto.of(id, cpf, exchange, averagePrice), null);
  }

  public static FindUserUseCaseOutput of(String message) {
    INotification notification = new Notification();
    notification.append(new NotificationErrorProps(message, USER_STR));
    return new FindUserUseCaseOutput(null, notification);
  }

}
