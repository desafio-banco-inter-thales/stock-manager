package com.desafio.application.user.find;

import com.desafio.application.user.IUserEntity;
import com.desafio.application.user.IUserGateway;
import com.desafio.application.user.UserDto;
import com.desafio.shared.log.ILog;
import com.desafio.shared.log.Log;

import java.util.Optional;

import static com.desafio.shared.utils.ActionConstants.FIND_BY_CPF;
import static com.desafio.shared.utils.ErrorConstants.GATEWAY_FIND_STR;
import static com.desafio.shared.utils.ErrorConstants.USER_DOES_NOT_EXISTS;
import static com.desafio.shared.utils.SuccessConstants.USER_FOUND;

public class FindUserUseCase {

  private static final ILog log = new Log(FindUserUseCase.class);

  private final IUserGateway userGateway;

  private FindUserUseCase(IUserGateway userGateway) {
    this.userGateway = userGateway;
  }

  public static FindUserUseCase of(IUserGateway userGateway) {
    return new FindUserUseCase(userGateway);
  }

  public FindUserUseCaseOutput execute(FindUserUseCaseCommand command) {
    log.info(FIND_BY_CPF, command.cpf());
    try {
      Optional<IUserEntity> optionalIUser = userGateway.findByCpf(command.cpf());

      if (optionalIUser.isEmpty()) {
        log.info(USER_DOES_NOT_EXISTS, command.cpf());
        return FindUserUseCaseOutput.of(USER_DOES_NOT_EXISTS.replace("{}", command.cpf()));
      }
      log.info(USER_FOUND, optionalIUser.get().toString());
      UserDto user = UserDto.of(optionalIUser.get());

      return FindUserUseCaseOutput.of(user.id(), user.cpf(), user.exchange(), user.averagePrice());
    } catch (Exception e) {
      log.error(e.getMessage());
      return FindUserUseCaseOutput.of(GATEWAY_FIND_STR.replace("{}", command.cpf()));
    }
  }
}
