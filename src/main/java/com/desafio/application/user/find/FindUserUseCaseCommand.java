package com.desafio.application.user.find;

public record FindUserUseCaseCommand(String cpf) {
  public static FindUserUseCaseCommand of(String userCpf) {
    return new FindUserUseCaseCommand(userCpf);
  }
}
