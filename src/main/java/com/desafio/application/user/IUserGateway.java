package com.desafio.application.user;

import java.util.Optional;

public interface IUserGateway {
  Optional<IUserEntity> findByCpf(String cpf);
  void save(UserDto user);
}
