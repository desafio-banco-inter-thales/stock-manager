package com.desafio.application.user.update;

import com.desafio.application.user.IUserGateway;
import com.desafio.application.user.UserDto;

public class UpdateUserUseCase {

  private final IUserGateway userGateway;

  private UpdateUserUseCase(IUserGateway userGateway) {
    this.userGateway = userGateway;
  }

  public static UpdateUserUseCase of(IUserGateway userGateway) {
    return new UpdateUserUseCase(userGateway);
  }

  public void execute(UpdateUserUseCaseCommand command) {
    UserDto user = UserDto.of(command.id(), command.cpf(), command.exchange(), command.averagePrice());
    this.userGateway.save(user);
  }
}
