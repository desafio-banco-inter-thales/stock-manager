package com.desafio.application.user.update;

import java.math.BigDecimal;

public record UpdateUserUseCaseCommand(String id, String cpf, BigDecimal exchange, BigDecimal averagePrice) {

  public static UpdateUserUseCaseCommand of(String id, String cpf, BigDecimal exchange, BigDecimal averagePrice) {
    return new UpdateUserUseCaseCommand(id, cpf, exchange, averagePrice);
  }

}
