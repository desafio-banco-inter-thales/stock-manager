package com.desafio.application.user.create;

public record CreateUserCommand(String cpf) {
  public static CreateUserCommand of(String cpf) {
    return new CreateUserCommand(cpf);
  }

}
