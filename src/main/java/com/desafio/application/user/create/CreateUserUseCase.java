package com.desafio.application.user.create;

import com.desafio.application.user.IUserEntity;
import com.desafio.application.user.IUserGateway;
import com.desafio.application.user.UserDto;
import com.desafio.domain.user.User;
import com.desafio.shared.log.ILog;
import com.desafio.shared.log.Log;

import java.util.Objects;

import static com.desafio.shared.utils.ErrorConstants.GATEWAY_INSERT_STR;
import static com.desafio.shared.utils.SuccessConstants.USER_INSERTED_ON_GATEWAY;

public class CreateUserUseCase {

  private static final ILog log = new Log(CreateUserUseCase.class);

  private final IUserGateway userGateway;

  private CreateUserUseCase(IUserGateway userGateway) {
    this.userGateway = Objects.requireNonNull(userGateway);
  }

  public static CreateUserUseCase of(IUserGateway userGateway) {
    return new CreateUserUseCase(userGateway);
  }

  public void execute(CreateUserCommand createUserCommand) {
    UserDto user = this.createUser(createUserCommand);

    if (user.notification().hasErrors())
      log.info(user.notification().messages(""));

    this.insertUser(UserDto.of(user));
  }

  private void insertUser(IUserEntity user) {
    try {
      this.userGateway.save(UserDto.of(user));
      log.info(USER_INSERTED_ON_GATEWAY, user.cpf());
    } catch (Exception e) {
      log.error(e.getMessage());
    }
  }

  private UserDto createUser(CreateUserCommand createUserCommand) {
    final String cpf = createUserCommand.cpf();

    User user = User.of(cpf);

    user.validate();

    return UserDto.of(user);
  }

}
