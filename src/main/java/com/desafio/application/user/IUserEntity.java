package com.desafio.application.user;

import java.math.BigDecimal;

public interface IUserEntity {
  String cpf();
  String id();
  BigDecimal exchange();
  BigDecimal averagePrice();
}
