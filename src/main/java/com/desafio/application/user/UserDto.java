package com.desafio.application.user;

import com.desafio.domain.user.IUser;
import com.desafio.domain.user.User;
import com.desafio.shared.notification.INotification;

import java.math.BigDecimal;

public record UserDto(String id, String cpf, BigDecimal exchange, BigDecimal averagePrice, INotification notification) implements IUserEntity {

  public static UserDto of(String id, String cpf, BigDecimal exchange, BigDecimal averagePrice, INotification notification) {
    return new UserDto(id, cpf, exchange, averagePrice, notification);
  }

  public static UserDto of(String id, String cpf, BigDecimal exchange, BigDecimal averagePrice) {
    return new UserDto(id, cpf, exchange, averagePrice, null);
  }

  public static UserDto of(IUser iUser) {
    return new UserDto(iUser.getIdStr(), iUser.getCpfStr(), iUser.getExchange(), iUser.getAveragePrice(), iUser.getNotification());
  }

  public static UserDto of(IUserEntity iUserEntity) {
    return new UserDto(iUserEntity.id(), iUserEntity.cpf(), iUserEntity.exchange(), iUserEntity.averagePrice(), null);
  }

  public IUser toAggregate() {
    return User.of(id, cpf, exchange, averagePrice);
  }

}
