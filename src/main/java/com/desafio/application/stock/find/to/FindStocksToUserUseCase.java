package com.desafio.application.stock.find.to;

import com.desafio.application.stock.gateway.IStockGateway;
import com.desafio.application.stock.gateway.StockResponseFind;
import com.desafio.domain.user.User;
import com.desafio.domain.user.cpf.Cpf;
import com.desafio.domain.user.cpf.CpfValidator;
import com.desafio.shared.notification.INotification;
import com.desafio.shared.notification.Notification;
import com.desafio.shared.notification.NotificationErrorProps;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.desafio.shared.utils.Constants.STOCK_STR;
import static com.desafio.shared.utils.ErrorConstants.USER_HAS_NO_STOCKS;
import static java.util.stream.Collectors.groupingBy;

public class FindStocksToUserUseCase {

  private final IStockGateway stockGateway;

  private FindStocksToUserUseCase(IStockGateway stockGateway) {
    this.stockGateway = stockGateway;
  }

  public static FindStocksToUserUseCase of(IStockGateway stockGateway) {
    return new FindStocksToUserUseCase(stockGateway);
  }

  public FindStocksToUserUseCaseOutput execute(FindStocksToUserUseCaseCommand command) {
    List<StockResponseFind> output = this.stockGateway.findAllStocksToUser(command.cpf());
    if(output.isEmpty())
      return FindStocksToUserUseCaseOutput.of(USER_HAS_NO_STOCKS.replace("{}", command.cpf()));

    Map<Object, List<StockResponseFind>> groupedStocks = output.stream().collect(groupingBy(StockResponseFind::getTicker));
    List<Response> response2s = new ArrayList<>();

    BigDecimal total = BigDecimal.ZERO;
    for (Object keys : groupedStocks.keySet()) {
      String ticker = keys.toString();
      List<StockResponseFind> stockResponseFindList = groupedStocks.get(keys);
      List<Map<Integer, BigDecimal>> prices = new ArrayList<>();
      Integer amount = 0;
      BigDecimal localTotal = BigDecimal.ZERO;
      for (StockResponseFind stockResponseFind : stockResponseFindList) {
        amount += stockResponseFind.getAmount();
        prices.add(Map.of(stockResponseFind.getAmount(), stockResponseFind.getPrice()));
        localTotal = localTotal.add(stockResponseFind.getPrice().multiply(BigDecimal.valueOf(stockResponseFind.getAmount())));
      }
      response2s.add(new Response(ticker, groupedStocks.get(keys).get(0).getActive(), amount, localTotal, prices));
      total = total.add(localTotal);
    }

    return FindStocksToUserUseCaseOutput.of(output.get(0).getExchange(), total, output.get(0).getAveragePrice(), response2s);
  }

}
