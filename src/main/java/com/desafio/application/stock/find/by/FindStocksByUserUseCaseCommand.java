package com.desafio.application.stock.find.by;

import com.desafio.application.user.UserDto;

public record FindStocksByUserUseCaseCommand(UserDto userDto) {

  public static FindStocksByUserUseCaseCommand of(UserDto userDto) {
    return new FindStocksByUserUseCaseCommand(userDto);
  }

}
