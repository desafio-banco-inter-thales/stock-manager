package com.desafio.application.stock.find.by;

import com.desafio.application.stock.mapper.FindAllTickersByUserMapper;

import java.util.List;

public record FindStocksByUserUseCaseOutput(List<FindAllTickersByUserMapper> findAllTickersByUserMapperList) {

  public static FindStocksByUserUseCaseOutput of(List<FindAllTickersByUserMapper> findAllTickersByUserMapperList) {
    return new FindStocksByUserUseCaseOutput(findAllTickersByUserMapperList);
  }

}
