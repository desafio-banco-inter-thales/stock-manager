package com.desafio.application.stock.find.by;

import com.desafio.application.stock.gateway.IStockGateway;
import com.desafio.application.stock.gateway.StockResponse;
import com.desafio.application.stock.mapper.FindAllTickersByUserMapper;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class FindStocksByUserUseCase {

  private final IStockGateway stockGateway;

  private FindStocksByUserUseCase(IStockGateway stockGateway) {
    this.stockGateway = stockGateway;
  }

  public static FindStocksByUserUseCase of(IStockGateway stockGateway) {
    return new FindStocksByUserUseCase(stockGateway);
  }

  public FindStocksByUserUseCaseOutput execute(FindStocksByUserUseCaseCommand command) {
    List<StockResponse> stockResponseList = this.stockGateway.findAllTickersByUser(command.userDto().id());

    List<FindAllTickersByUserMapper> list = new ArrayList<>();
    for (StockResponse stockResponse : stockResponseList) {
      FindAllTickersByUserMapper of = FindAllTickersByUserMapper.of(stockResponse.getTicker(), stockResponse.getAmount(), stockResponse.getCompany(), stockResponse.getPrice(), stockResponse.getExchange());
      list.add(of);
    }
    return FindStocksByUserUseCaseOutput.of(list);
  }
}
