package com.desafio.application.stock.find.to;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public record Response(String ticker, Boolean active, Integer amount, BigDecimal total, List<Map<Integer, BigDecimal>> historyAmountXPrice) {

  public static Response of(String ticker, Boolean active, Integer amount, BigDecimal total, List<Map<Integer, BigDecimal>> historyAmountXPrice) {
    return new Response(ticker, active, amount, total, historyAmountXPrice);
  }

}
