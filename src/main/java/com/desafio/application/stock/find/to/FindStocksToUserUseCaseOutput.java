package com.desafio.application.stock.find.to;

import com.desafio.shared.notification.INotification;
import com.desafio.shared.notification.Notification;
import com.desafio.shared.notification.NotificationErrorProps;

import java.math.BigDecimal;
import java.util.List;

import static com.desafio.shared.utils.Constants.STOCK_STR;
import static com.desafio.shared.utils.ErrorConstants.USER_HAS_NO_STOCKS;

public record FindStocksToUserUseCaseOutput(BigDecimal exchange, BigDecimal total, BigDecimal averagePrice, List<Response> stocks, INotification notification) {

  public static FindStocksToUserUseCaseOutput of(BigDecimal exchange, BigDecimal total, BigDecimal averagePrice, List<Response> stocks) {
    return new FindStocksToUserUseCaseOutput(exchange, total, averagePrice, stocks, null);
  }

  public static FindStocksToUserUseCaseOutput of(INotification notification) {
    return new FindStocksToUserUseCaseOutput(null, null, null, null, notification);
  }

  public static FindStocksToUserUseCaseOutput of(String message) {
    INotification notification = new Notification();
    notification.append(new NotificationErrorProps(message, STOCK_STR));
    return new FindStocksToUserUseCaseOutput(null, null, null, null, notification);
  }

  public static FindStocksToUserUseCaseOutput of(String message, String context) {
    INotification notification = new Notification();
    notification.append(new NotificationErrorProps(message, context));
    return new FindStocksToUserUseCaseOutput(null, null, null, null, notification);
  }

}
