package com.desafio.application.stock.find.to;

public record FindStocksToUserUseCaseCommand(String cpf) {

  public static FindStocksToUserUseCaseCommand of(String cpf) {
    return new FindStocksToUserUseCaseCommand(cpf);
  }

}
