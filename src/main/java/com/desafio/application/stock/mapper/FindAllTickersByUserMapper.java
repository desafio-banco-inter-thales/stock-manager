package com.desafio.application.stock.mapper;

import com.desafio.application.company.ICompanyEntity;

import java.math.BigDecimal;

public record FindAllTickersByUserMapper(String ticker, Integer amount, ICompanyEntity company, BigDecimal price, BigDecimal total, BigDecimal exchange) {

  public static FindAllTickersByUserMapper of(String ticker, Integer amount, ICompanyEntity company, BigDecimal price, BigDecimal exchange) {
    return new FindAllTickersByUserMapper(ticker, amount, company, price, price.multiply(BigDecimal.valueOf(amount)), exchange);
  }

}
