package com.desafio.application.stock.buy;

import com.desafio.application.stock.StockDto;

import java.math.BigDecimal;
import java.util.List;

public record BuyUserStocksUseCaseOutput(List<StockDto> stocks, BigDecimal exchange, BigDecimal averagePrice) {

  public static BuyUserStocksUseCaseOutput of(List<StockDto> stocks, BigDecimal exchange, BigDecimal averagePrice) {
    return new BuyUserStocksUseCaseOutput(stocks, exchange, averagePrice);
  }

  public static BuyUserStocksUseCaseOutput of(BigDecimal exchange) {
    return new BuyUserStocksUseCaseOutput(null, exchange, null);
  }
}