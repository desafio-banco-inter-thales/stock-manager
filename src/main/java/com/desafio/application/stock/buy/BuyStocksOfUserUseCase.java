package com.desafio.application.stock.buy;

import com.desafio.application.stock.GetAveragePriceUseCase;
import com.desafio.application.stock.SortListOfStocksUseCase;
import com.desafio.application.stock.StockDto;
import com.desafio.domain.stock.IStock;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Comparator;
import java.util.List;

import static java.math.BigDecimal.ZERO;
import static java.math.BigDecimal.valueOf;

public class BuyStocksOfUserUseCase {

  List<IStock> stocksToInvest;
  BigDecimal cumulativeUntilReachOrder;

  private final GetAveragePriceUseCase getAveragePriceUseCase;

  private final SortListOfStocksUseCase sortListOfStocksUseCase;

  private BuyStocksOfUserUseCase(GetAveragePriceUseCase getAveragePriceUseCase, SortListOfStocksUseCase sortListOfStocksUseCase) {
    this.getAveragePriceUseCase = getAveragePriceUseCase;
    this.sortListOfStocksUseCase = sortListOfStocksUseCase;
  }

  public static BuyStocksOfUserUseCase of(
          GetAveragePriceUseCase getAveragePriceUseCase,
          SortListOfStocksUseCase sortListOfStocksUseCase
  ) {
    return new BuyStocksOfUserUseCase(getAveragePriceUseCase, sortListOfStocksUseCase);
  }

  public BuyUserStocksUseCaseOutput execute(BuyUserStocksUseCaseCommand command) {
    var order = command.order();
    final var previousAveragePrice = command.averagePrice();
    var stocks = command.stockDtoList().stream().map(StockDto::toAggregate).toList();
    if(stocks.isEmpty())
      return null;

    stocks = sortListOfStocksUseCase.getStocksToBalance(stocks, previousAveragePrice);
    BigDecimal limiter = order.divide(valueOf(stocks.size()), 2, RoundingMode.UP);

    for (int i = 0; i < stocks.size(); i++) {
      IStock stock = stocks.get(i);

      if(order.compareTo(ZERO) < 0)
        break;

      if(previousAveragePrice.intValue() != 0 && stock.getTotal().divide(previousAveragePrice, 3, RoundingMode.UP).compareTo(valueOf(0.988)) < 0 && limiter.compareTo(order) < 0) {
        limiter =  previousAveragePrice.subtract(stock.getTotal()).add(limiter);
        if (limiter.compareTo(order) > 0)
          limiter = order;
        if (limiter.compareTo(previousAveragePrice) > 0)
          limiter = previousAveragePrice;
      }

      int amount = limiter.divideToIntegralValue(stock.getPrice()).intValue();
      stock.buy(amount);
      order = order.subtract(stock.getTotal());
      if (i+1 < stocks.size())
        limiter = order.divide(valueOf(stocks.subList(i+1, stocks.size()).size()), 2, RoundingMode.UP);
    }

    return buyMoreStocksWithReaminingMoney(order, stocks);
  }

  private BuyUserStocksUseCaseOutput buyMoreStocksWithReaminingMoney(BigDecimal rest, List<IStock> stocks) {
    int iterator = 0;
    int breakCondition = stocks.size() - 1;
    List<IStock> stocksToBalance = stocks.stream().sorted(Comparator.comparing(IStock::getPrice)).toList();
    if (!stocksToBalance.isEmpty())
      stocks = stocksToBalance;

    while (rest.compareTo(ZERO) > 0) {
      rest = rest.subtract(stocks.get(iterator).getPrice());
      stocks.get(iterator).buy(1);

      if (rest.compareTo(BigDecimal.ZERO) < 0) {
        rest = rest.add(stocks.get(iterator).getPrice());
        stocks.get(iterator).sell(1);
        breakCondition = iterator - 1;
      }

      iterator += 1;
      if (iterator > breakCondition)
        iterator = 0;
      if (breakCondition < 0)
        break;
    }

    final BigDecimal averagePrice = getAveragePriceUseCase.execute(stocksToBalance);
    if (averagePrice == null) return null;

    List<StockDto> finalStocks = stocksToBalance.stream().map(StockDto::of).toList();
    return BuyUserStocksUseCaseOutput.of(finalStocks, rest, averagePrice);
  }


}
