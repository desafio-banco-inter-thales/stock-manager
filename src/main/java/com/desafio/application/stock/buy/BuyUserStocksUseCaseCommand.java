package com.desafio.application.stock.buy;

import com.desafio.application.stock.StockDto;

import java.math.BigDecimal;
import java.util.List;

public record BuyUserStocksUseCaseCommand(BigDecimal order, List<StockDto> stockDtoList, BigDecimal averagePrice) {

  public static BuyUserStocksUseCaseCommand of(BigDecimal order, List<StockDto> stockDtoList, BigDecimal averagePrice) {
    return new BuyUserStocksUseCaseCommand(order, stockDtoList, averagePrice);
  }

}
