package com.desafio.application.stock;

import com.desafio.application.company.CompanyDto;
import com.desafio.application.stock.gateway.IStockEntity;
import com.desafio.application.user.UserDto;
import com.desafio.domain.stock.IStock;
import com.desafio.domain.stock.Stock;
import com.desafio.shared.notification.INotification;

import java.math.BigDecimal;
import java.time.Instant;

public record StockDto(
        String id,
        UserDto user,
        CompanyDto company,
        Integer amount,
        BigDecimal price,
        BigDecimal total,
        Instant createdAt,
        Instant updatedAt,
        Instant deletedAt,
        INotification notification) implements IStockEntity {

  public static StockDto of(String id, UserDto userDto, CompanyDto companyDto, Integer amount, BigDecimal price, Instant createdAt, Instant updatedAt, Instant deletedAt, INotification notification) {
    return new StockDto(id, userDto, companyDto, amount, price, null, createdAt, updatedAt, deletedAt, notification);
  }

  public static StockDto of(UserDto userDto, CompanyDto companyDto, BigDecimal price) {
    return new StockDto(null, userDto, companyDto, 0, price, BigDecimal.ZERO, null, null, null, null);
  }

  public static StockDto of(UserDto userDto, CompanyDto companyDto, BigDecimal price, BigDecimal total) {
    return new StockDto(null, userDto, companyDto, 0, price, total, null, null, null, null);
  }

  public static StockDto of(UserDto userDto, CompanyDto companyDto, Integer amount, BigDecimal price) {
    return new StockDto(null, userDto, companyDto, amount, price, null, null, null, null, null);
  }

  public static StockDto of(UserDto userDto, CompanyDto companyDto, Integer amount, BigDecimal price, BigDecimal newPrice) {
    return new StockDto(null, userDto, companyDto, amount, price, newPrice, null, null, null, null);
  }

  public static StockDto of(IStock iStock) {
    return new StockDto(iStock.getIdStr(), UserDto.of(iStock.getUser()), CompanyDto.of(iStock.getCompany()), iStock.getAmount(), iStock.getPrice(), iStock.getTotal(), iStock.getCreatedAt(), iStock.getUpdatedAt(), iStock.getDeletedAt(), null);
  }

  public static StockDto update(Integer amount, StockDto stockDto) {
    return new StockDto(stockDto.id(), stockDto.user(), stockDto.company(), amount, stockDto.price(), null, stockDto.createdAt(), stockDto.updatedAt(), stockDto.deletedAt(), stockDto.notification());
  }

  public IStockEntity toEntity(IStock iStock) {
    return new StockDto(iStock.getIdStr(), UserDto.of(iStock.getUser()), CompanyDto.of(iStock.getCompany()), iStock.getAmount(), iStock.getPrice(), null, iStock.getCreatedAt(), iStock.getUpdatedAt(), iStock.getDeletedAt(), null);
  }

  public IStock toAggregate() {
    return Stock.of(id, user.toAggregate(), company.toAggregate(), amount, price, total);
  }

}
