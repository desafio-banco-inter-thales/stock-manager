package com.desafio.application.stock.choose;

import com.desafio.application.stock.StockDto;

import java.util.List;

public record ChooseStocksToBuyUseCaseOutput(List<StockDto> stockDtoList) {

  public static ChooseStocksToBuyUseCaseOutput of(List<StockDto> stockDtoList) {
    return new ChooseStocksToBuyUseCaseOutput(stockDtoList);
  }

}
