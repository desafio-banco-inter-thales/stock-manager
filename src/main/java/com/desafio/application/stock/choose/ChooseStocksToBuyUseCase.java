package com.desafio.application.stock.choose;

import com.desafio.application.company.CompanyDto;
import com.desafio.application.company.ICompanyEntity;
import com.desafio.application.company.find.all.FindAllActiveCompaniesUseCase;
import com.desafio.application.company.find.all.FindAllCompaniesUserAlreadyHasUseCase;
import com.desafio.application.company.find.all.FindAllCompaniesUserDoesNotHaveUseCase;
import com.desafio.application.stock.StockDto;
import com.desafio.application.stock.mapper.FindAllTickersByUserMapper;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ChooseStocksToBuyUseCase {

  private final FindAllCompaniesUserAlreadyHasUseCase findAllCompaniesUserAlreadyHasUseCase;
  private final FindAllCompaniesUserDoesNotHaveUseCase findAllCompaniesUserDoesNotHaveUseCase;
  private final FindAllActiveCompaniesUseCase findAllActiveCompaniesUseCase;

  private ChooseStocksToBuyUseCase(
          FindAllCompaniesUserAlreadyHasUseCase findAllCompaniesUserAlreadyHasUseCase,
          FindAllCompaniesUserDoesNotHaveUseCase findAllCompaniesUserDoesNotHaveUseCase,
          FindAllActiveCompaniesUseCase findAllActiveCompaniesUseCase) {
    this.findAllCompaniesUserAlreadyHasUseCase = findAllCompaniesUserAlreadyHasUseCase;
    this.findAllCompaniesUserDoesNotHaveUseCase = findAllCompaniesUserDoesNotHaveUseCase;
    this.findAllActiveCompaniesUseCase = findAllActiveCompaniesUseCase;
  }


  public static ChooseStocksToBuyUseCase of(
          FindAllCompaniesUserAlreadyHasUseCase findAllCompaniesUserAlreadyHasUseCase,
          FindAllCompaniesUserDoesNotHaveUseCase findAllCompaniesUserDoesNotHaveUseCase,
          FindAllActiveCompaniesUseCase findAllActiveCompaniesUseCase
  ) {
    return new ChooseStocksToBuyUseCase(findAllCompaniesUserAlreadyHasUseCase, findAllCompaniesUserDoesNotHaveUseCase, findAllActiveCompaniesUseCase);
  }


  public ChooseStocksToBuyUseCaseOutput execute(ChooseStocksToBuyUseCaseCommand command) {
    List<FindAllTickersByUserMapper> mapperList = command.findAllTickersByUserMapperList();
    List<StockDto> stockDtos = new ArrayList<>();
    Map<String, BigDecimal> previousStocks = mapperList.stream().collect(Collectors.toMap(FindAllTickersByUserMapper::ticker, FindAllTickersByUserMapper::total));

    if(mapperList.isEmpty()) {
      List<ICompanyEntity> companies = findAllActiveCompaniesUseCase.execute().companies();
      stockDtos = companies.stream().map(company ->
              StockDto.of(command.userDto(), CompanyDto.of(company), company.price())
      ).toList();

      if(stockDtos.size() > command.amount())
        stockDtos = stockDtos.subList(0, command.amount());

      return ChooseStocksToBuyUseCaseOutput.of(stockDtos);
    }

    List<ICompanyEntity> companiesUserHas = findAllCompaniesUserAlreadyHasUseCase.execute(previousStocks.keySet().stream().toList()).companies();
    List<ICompanyEntity> companiesUserDoesNotHave = findAllCompaniesUserDoesNotHaveUseCase.execute(previousStocks.keySet().stream().toList()).companies();

    stockDtos.addAll(companiesUserHas.stream().map(company ->
            StockDto.of(command.userDto(), CompanyDto.of(company), company.price(), previousStocks.get(company.ticker()))
    ).toList());

    stockDtos.addAll(companiesUserDoesNotHave.stream().map(company ->
            StockDto.of(command.userDto(), CompanyDto.of(company), company.price())
    ).toList());

    if(stockDtos.size() > command.amount())
      stockDtos = stockDtos.subList(0, command.amount());

    return ChooseStocksToBuyUseCaseOutput.of(stockDtos);
  }

}
