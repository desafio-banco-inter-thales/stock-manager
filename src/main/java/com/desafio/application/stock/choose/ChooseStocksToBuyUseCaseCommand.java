package com.desafio.application.stock.choose;

import com.desafio.application.stock.mapper.FindAllTickersByUserMapper;
import com.desafio.application.user.UserDto;

import java.util.List;

public record ChooseStocksToBuyUseCaseCommand(List<FindAllTickersByUserMapper> findAllTickersByUserMapperList, UserDto userDto, Integer amount) {

  public static ChooseStocksToBuyUseCaseCommand of(List<FindAllTickersByUserMapper> findAllTickersByUserMapperList, UserDto userDto, Integer amount) {
    return new ChooseStocksToBuyUseCaseCommand(findAllTickersByUserMapperList, userDto, amount);
  }

}
