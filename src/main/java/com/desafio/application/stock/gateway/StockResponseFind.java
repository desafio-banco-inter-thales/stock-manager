package com.desafio.application.stock.gateway;

import java.math.BigDecimal;

public interface StockResponseFind {

  String getTicker();
  Integer getAmount();
  BigDecimal getPrice();
  Boolean getActive();
  BigDecimal getExchange();
  BigDecimal getAveragePrice();

}
