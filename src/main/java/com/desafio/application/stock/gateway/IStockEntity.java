package com.desafio.application.stock.gateway;

import com.desafio.application.company.ICompanyEntity;
import com.desafio.application.user.IUserEntity;

import java.math.BigDecimal;
import java.time.Instant;

public interface IStockEntity {
  ICompanyEntity company()  ;
  String id();
  IUserEntity user();
  BigDecimal price();
  Integer amount();
  Instant createdAt();
  Instant updatedAt();
  Instant deletedAt();
}
