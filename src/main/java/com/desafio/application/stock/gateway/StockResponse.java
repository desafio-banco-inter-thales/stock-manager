package com.desafio.application.stock.gateway;

import com.desafio.application.company.ICompanyEntity;

import java.math.BigDecimal;

public interface StockResponse {

  String getTicker();
  Integer getAmount();
  ICompanyEntity getCompany();
  BigDecimal getPrice();
  BigDecimal getExchange();

}
