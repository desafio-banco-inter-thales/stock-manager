package com.desafio.application.stock.gateway;

import java.util.List;

public interface IStockGateway<T extends IStockEntity> {

  List<StockResponse> findAllTickersByUser(String id);

  List<StockResponseFind> findAllStocksToUser(String cpf);

  void deleteAllByCompanyTicker(String ticker);

  void createAll(List<T> stocks);
}
