package com.desafio.application.stock;

import com.desafio.domain.stock.IStock;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

import static java.math.BigDecimal.*;

public class GetAveragePriceUseCase {

  private GetAveragePriceUseCase() {

  }

  public static GetAveragePriceUseCase of() {
    return new GetAveragePriceUseCase();
  }

  public BigDecimal execute(List<IStock> stocks) {
    BigDecimal total = ZERO;
    BigDecimal amount = ZERO;
    for (IStock iStock : stocks) {
      if(iStock.getTotal() != null) {
        total = total.add(iStock.getTotal());
        amount = amount.add(ONE);
      }
    }

    if(amount.equals(ZERO))
      return ZERO;

    return total.divide(amount, 2, RoundingMode.HALF_UP);
  }

}
