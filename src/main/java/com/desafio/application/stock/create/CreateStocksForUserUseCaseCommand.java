package com.desafio.application.stock.create;

import com.desafio.application.stock.StockDto;

import java.util.List;
import java.util.Map;

public record CreateStocksForUserUseCaseCommand(List<StockDto> stocks) {

  public static CreateStocksForUserUseCaseCommand of(List<StockDto> stocks) {
    return new CreateStocksForUserUseCaseCommand(stocks);
  }

}
