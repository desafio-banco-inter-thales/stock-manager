package com.desafio.application.stock.create;

import com.desafio.application.stock.StockDto;
import com.desafio.application.stock.gateway.IStockGateway;

import java.util.ArrayList;

public class CreateStocksOfUserUseCase {

  private final IStockGateway stockGateway;

  private CreateStocksOfUserUseCase(IStockGateway stockGateway) {
    this.stockGateway = stockGateway;
  }

  public static CreateStocksOfUserUseCase of(IStockGateway stockGateway) {
    return new CreateStocksOfUserUseCase(stockGateway);
  }

  public void execute(CreateStocksForUserUseCaseCommand command) {
    final var stocks = command.stocks();
    final var stocksToCreate = new ArrayList<StockDto>();
    for (StockDto stockDto : stocks) {
      String ticker = stockDto.company().ticker();
      Integer amount = stockDto.amount();
      if(amount > 0)
        stocksToCreate.add(StockDto.update(amount, stockDto));
    }

    this.stockGateway.createAll(stocksToCreate);
  }
}
