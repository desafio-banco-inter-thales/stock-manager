package com.desafio.application.stock.delete;

import com.desafio.application.company.delete.DeleteCompanyCommand;
import com.desafio.application.stock.gateway.IStockGateway;
import com.desafio.shared.log.ILog;
import com.desafio.shared.log.Log;

import static com.desafio.shared.utils.ActionConstants.DELETE_STOCKS;
import static com.desafio.shared.utils.SuccessConstants.STOCKS_REMOVED_ON_GATEWAY;

public class DeleteAllStocksByCompanyTickerUseCase {

  private static final ILog log = new Log(DeleteAllStocksByCompanyTickerUseCase.class);

  private final IStockGateway stockGateway;

  private DeleteAllStocksByCompanyTickerUseCase(IStockGateway stockGateway) {
    this.stockGateway = stockGateway;
  }


  public static DeleteAllStocksByCompanyTickerUseCase of(IStockGateway stockGateway) {
    return new DeleteAllStocksByCompanyTickerUseCase(stockGateway);
  }

  public void execute(DeleteAllStocksByCompanyTickerCommand command) {
    log.info(DELETE_STOCKS, command.companyTicker());
    try {
      this.stockGateway.deleteAllByCompanyTicker(command.companyTicker());
      log.info(STOCKS_REMOVED_ON_GATEWAY, command.companyTicker());
    } catch (Exception e) {
      log.error(e.getMessage());
    }
  }
}
