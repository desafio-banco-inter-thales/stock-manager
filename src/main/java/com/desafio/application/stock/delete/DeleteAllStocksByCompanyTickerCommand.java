package com.desafio.application.stock.delete;

public record DeleteAllStocksByCompanyTickerCommand(String companyTicker) {

  public static DeleteAllStocksByCompanyTickerCommand of(String companyTicker) {
    return new DeleteAllStocksByCompanyTickerCommand(companyTicker);
  }

}
