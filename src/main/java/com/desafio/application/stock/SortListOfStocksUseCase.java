package com.desafio.application.stock;

import com.desafio.domain.stock.IStock;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import static java.math.BigDecimal.ZERO;
import static java.math.BigDecimal.valueOf;

public class SortListOfStocksUseCase {

  private final GetAveragePriceUseCase getAveragePriceUseCase;

  private SortListOfStocksUseCase(GetAveragePriceUseCase getAveragePriceUseCase) {
    this.getAveragePriceUseCase = getAveragePriceUseCase;
  }

  public static SortListOfStocksUseCase of(GetAveragePriceUseCase getAveragePriceUseCase) {
    return new SortListOfStocksUseCase(getAveragePriceUseCase);
  }


  public List<StockDto> executeFromDto(List<StockDto> stockDtoList, BigDecimal previousAveragePrice) {
    if(previousAveragePrice.compareTo(BigDecimal.ZERO) < 0)
      return null;

    final var stocks = stockDtoList.stream().map(StockDto::toAggregate).toList();
    List<IStock> stockList = execute(stocks, previousAveragePrice);
    return stockList.stream().map(StockDto::of).toList();
  }

  public List<IStock> execute(List<IStock> stocks, BigDecimal previousAveragePrice) {
    final BigDecimal averagePrice = getAveragePriceUseCase.execute(stocks);
    if (averagePrice == null) return null;

    if(previousAveragePrice.equals(ZERO))
      return stocks.stream().sorted(Comparator.comparing(IStock::getPrice)).toList();

    if (averagePrice.compareTo(previousAveragePrice) >= 0)
      return stocks.stream().sorted(Comparator.comparing(IStock::getTotal)).toList();

    final var finalStocks = new ArrayList<IStock>();
    List<IStock> stocksToBalance = stocks.stream().filter(stock ->
            stock.getTotal().divide(previousAveragePrice, 2, RoundingMode.HALF_UP).compareTo(valueOf(0.97)) <= 0
    ).toList();
    List<IStock> balancedStocks = stocks.stream().filter(stock ->
            stock.getTotal().divide(previousAveragePrice, 2, RoundingMode.HALF_UP).compareTo(valueOf(0.97)) > 0
    ).toList();
    stocksToBalance = stocksToBalance.stream().sorted(Comparator.comparing(IStock::getPrice)).toList();
    balancedStocks = balancedStocks.stream().sorted(Comparator.comparing(IStock::getPrice)).toList();
    finalStocks.addAll(stocksToBalance);
    finalStocks.addAll(balancedStocks);
    return finalStocks;
  }

  public List<IStock> getStocksToBalance(List<IStock> stocks, BigDecimal previousAveragePrice) {
    final BigDecimal averagePrice = getAveragePriceUseCase.execute(stocks);
    if (averagePrice == null) return null;

    if(previousAveragePrice.equals(ZERO))
      return stocks.stream().sorted(Comparator.comparing(IStock::getPrice)).toList();

    return stocks.stream().sorted(Comparator.comparing(IStock::getTotal)).toList();
  }


}
