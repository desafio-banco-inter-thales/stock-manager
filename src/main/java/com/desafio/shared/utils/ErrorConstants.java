package com.desafio.shared.utils;

public class ErrorConstants {

  public static final String STRING_SHOULD_NOT_BE_NULL = "'{}' SHOULD NOT BE NULL.";
  public static final String STRING_SHOULD_NOT_BE_BLANK = "'{}' SHOULD NOT BE BLANK.";
  public static final String CPF_INVALID_FORMAT = "'CPF' INVALID FORMAT {}.";
  public static final String CPF_INVALID = "'CPF' IS INVALID {}.";
  public static final String NAME_LENGTH_INVALID = "'NAME' MUST BE BETWEEN 3 AND 30 CHARACTERS.";
  public static final String EXCHANGE_MUST_NOT_BE_NEGATIVE = "'EXCHANGE' MUST NOT BE NEGATIVE";
  public static final String AVERAGE_PRICE_MUST_NOT_BE_NEGATIVE = "'AVERAGE_PRICE' MUST NOT BE NEGATIVE";
  public static final String TICKER_LENGTH_INVALID = "'TICKER' MUST BE BETWEEN 5 AND 7 CHARACTERS.";
  public static final String PRICE_INVALID = "'PRICE' MUST BE BIGGER THAN 0.";
  public static final String NEGATIVE_AMOUNT_OF_STOCKS = "YOU CAN NOT HAVE NEGATIVE AMOUNT OF STOCKS";
  public static final String NEGATIVE_PRICE_OF_STOCK = "YOU CAN NOT HAVE NEGATIVE PRICE OF STOCK";
  public static final String GATEWAY_FIND_STR = "COULD NOT FIND OBJECT {}";
  public static final String COMPANY_DOES_NOT_EXISTS = "COMPANY WITH TICKER '{}' NOT FOUND.";
  public static final String COMPANY_ALREADY_EXISTS = "COMPANY WITH TICKER: {} ALREADY EXISTS.";
  public static final String USER_HAS_NO_STOCKS = "USER WITH CPF '{}' HAS NO STOCKS.";
  public static final String USER_NOT_FOUND = "USER WITH CPF '{}' NOT FOUND.";
  public static final String CANNOT_BUY_ZERO_STOCKS = "YOU CANNOT BUY 0 STOCKS.";
  public static final String THERE_ARE_NO_ACTIVE_STOCKS = "THERE ARE NO ACTIVE STOCKS.";
  public static final String COMPANY_NOT_DELETED_BC_IT_DOES_NOT_EXIST = "COMPANY WITH TICKER '{}' NOT DELETE BECAUSE IT WAS NOT FOUND.";
  public static final String GATEWAY_DELETE_STR = "COULD NOT DELETE OBJECT {}";
  public static final String NO_STOCKS_TO_BUY = "THERE ARE NO STOCKS OF COMPANIES TO BUY. YOU SHOULD CREATE COMPANIES BEFORE TRY TO BUY STOCKS :)";
  public static final String GATEWAY_INSERT_STR = "COULD NOT INSERT OBJECT {}";
  public static final String USER_DOES_NOT_EXISTS = "USER WITH CPF {} NOT FOUND.";

}
