package com.desafio.shared.utils;

public class ActionConstants {
  public static final String FIND_COMPANY = "GETTING COMPANY WITH TICKER: '{}'...";
  public static final String CREATE_COMPANY = "CREATING COMPANY: '{}'...";
  public static final String DELETE_COMPANY = "DELETING COMPANY WITH TICKER: '{}'...";
  public static final String DELETE_STOCKS = "DELETING ALL STOCKS BY COMPANY WITH TICKER: '{}'...";
  public static final String FIND_BY_CPF = "GETTING USER BY CPF {}...";
  public static final String GETTING_ALL_ACTIVE_COMPANIES = "GETTING ALL ACTIVE COMPANIES";
  public static final String GETTING_ALL_COMPANIES_USER_ALREADY_HAS = "GETTING ALL COMPANIES USER ALREADY HAS";
  public static final String GETTING_ALL_COMPANIES_USER_DOES_NOT_HAVE = "GETTING ALL COMPANIES USER DOES NOT HAVE";
}
