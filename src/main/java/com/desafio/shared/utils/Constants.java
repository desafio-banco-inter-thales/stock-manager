package com.desafio.shared.utils;

public class Constants {
  public static final String USER_STR = "USER";
  public static final String COMPANY_STR = "COMPANY";
  public static final String STOCK_STR = "STOCK";
  public static final String CPF_STR = "CPF";
  public static final String NAME_STR = "NAME";
  public static final String EXCHANGE_STR = "EXCHANGE";
  public static final String TICKER_STR = "TICKER";
  public static final String PRICE_STR = "PRICE";


  public static final Integer MAX_NAME_LEN = 30;
  public static final Integer MIN_NAME_LEN = 3;
  public static final Integer MAX_TICKER_LEN = 7;
  public static final Integer MIN_TICKER_LEN = 5;
}
