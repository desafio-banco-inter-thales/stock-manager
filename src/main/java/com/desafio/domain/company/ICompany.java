package com.desafio.domain.company;

import com.desafio.shared.notification.INotification;

import java.math.BigDecimal;

public interface ICompany {
  String getIdStr();
  String getTickerStr();
  String getName();
  BigDecimal getPrice();
  Boolean getActive();
  INotification getNotification();
}
