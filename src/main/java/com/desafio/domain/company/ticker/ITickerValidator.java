package com.desafio.domain.company.ticker;


import com.desafio.shared.notification.INotification;

public interface ITickerValidator {
  public abstract void validate(ITicker ticker, INotification notification);
}
