package com.desafio.domain.company;

import com.desafio.domain.Aggregate;
import com.desafio.domain.company.ticker.ITicker;
import com.desafio.domain.company.ticker.Ticker;
import com.desafio.domain.company.ticker.TickerValidator;
import com.desafio.shared.notification.INotification;
import com.desafio.shared.notification.Notification;

import java.math.BigDecimal;

public class Company extends Aggregate<CompanyId> implements ICompany {

  private String name;
  private ITicker ticker;
  private Boolean isActive;

  protected Company(String id, String name, ITicker ticker, Boolean isActive, INotification notification) {
    super(id == null ? CompanyId.unique() : CompanyId.from(id), notification);
    this.name = name;
    this.isActive = isActive;
    this.ticker = ticker;
  }

  public static Company of(String id, String name, String ticker, BigDecimal price, Boolean isActive) {
    INotification notification = new Notification();
    ITicker tickerObj = Ticker.of(ticker, price);
    return new Company(id, name, tickerObj, isActive, notification);
  }

  public static Company of(String name, String ticker, BigDecimal price, Boolean isActive) {
    INotification notification = new Notification();
    ITicker tickerObj = Ticker.of(ticker, price);
    return new Company(null, name, tickerObj, isActive, notification);
  }

  @Override
  public void validate() {
    new CompanyValidator(new TickerValidator()).validate(this);
  }

  @Override
  public String getIdStr() {
    return this.id.getValue();
  }

  @Override
  public String getTickerStr() {
    return this.getTicker().getValue();
  }

  public String getName() {
    return name;
  }

  @Override
  public BigDecimal getPrice() {
    return this.getTicker().getPrice();
  }

  public ITicker getTicker() {
    return ticker;
  }

  public Boolean getActive() {
    return isActive;
  }
}
