package com.desafio.domain.user;

import com.desafio.domain.Validator;
import com.desafio.domain.user.afterValidation.IAfterValidation;
import com.desafio.domain.user.cpf.ICpfValidator;

import java.math.BigDecimal;
import java.util.List;

import static com.desafio.shared.utils.Constants.USER_STR;
import static com.desafio.shared.utils.ErrorConstants.AVERAGE_PRICE_MUST_NOT_BE_NEGATIVE;
import static com.desafio.shared.utils.ErrorConstants.EXCHANGE_MUST_NOT_BE_NEGATIVE;

public class UserValidator extends Validator<User> {

  private final ICpfValidator cpfValidator;

  private final List<IAfterValidation<User>> validationHandlers;

  public UserValidator(ICpfValidator cpfValidator, List<IAfterValidation<User>> validationHandlers) {
    this.cpfValidator = cpfValidator;
    this.validationHandlers = validationHandlers;
  }

  @Override
  public void validate(User user) {
    if (user.getExchange().compareTo(BigDecimal.ZERO) < 0)
      user.getNotification().append(EXCHANGE_MUST_NOT_BE_NEGATIVE, USER_STR);
    if (user.getAveragePrice().compareTo(BigDecimal.ZERO) < 0)
      user.getNotification().append(AVERAGE_PRICE_MUST_NOT_BE_NEGATIVE, USER_STR);

    this.cpfValidator.validate(user.getCpf(), user.getNotification());

    if(!user.getNotification().hasErrors())
      this.validationHandlers.forEach(handler -> handler.execute(user));
  }

}
