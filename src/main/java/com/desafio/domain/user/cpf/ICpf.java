package com.desafio.domain.user.cpf;

public interface ICpf {
  public String getValue();
}
