package com.desafio.domain.user;

import com.desafio.domain.Aggregate;
import com.desafio.domain.user.afterValidation.IAfterValidation;
import com.desafio.domain.user.afterValidation.formatCpf.FormatCpf;
import com.desafio.domain.user.cpf.Cpf;
import com.desafio.domain.user.cpf.CpfValidator;
import com.desafio.domain.user.cpf.ICpf;
import com.desafio.shared.notification.INotification;
import com.desafio.shared.notification.Notification;

import java.math.BigDecimal;
import java.util.List;

public class User extends Aggregate<UserId> implements IUser {

  private ICpf cpf;
  private BigDecimal exchange;
  private BigDecimal averagePrice;

  protected User(String id, ICpf cpf, BigDecimal exchange, BigDecimal averagePrice, INotification notification) {
    super(id == null? UserId.unique() : UserId.from(id), notification);
    this.cpf = cpf;
    this.exchange = exchange;
    this.averagePrice = averagePrice;
  }

  public static User of(String cpf) {
    INotification notification = new Notification();
    Cpf cpfObj = Cpf.of(cpf);
    return new User(null, cpfObj, BigDecimal.ZERO, BigDecimal.ZERO, notification);
  }

  public static User of(String cpf, BigDecimal exchange, BigDecimal averagePrice) {
    INotification notification = new Notification();
    Cpf cpfObj = Cpf.of(cpf);
    return new User(null, cpfObj, exchange, averagePrice, notification);
  }

  public static User of(String id, String cpf, BigDecimal exchange, BigDecimal averagePrice) {
    INotification notification = new Notification();
    Cpf cpfObj = Cpf.of(cpf);
    return new User(id, cpfObj, exchange, averagePrice, notification);
  }


  public ICpf getCpf() {
    return cpf;
  }

  @Override
  public String getIdStr() {
    return id.getValue();
  }

  @Override
  public String getCpfStr() {
    return cpf.getValue();
  }

  @Override
  public BigDecimal getExchange() {
    return exchange;
  }

  @Override
  public BigDecimal getAveragePrice() {
    return averagePrice;
  }

  public void formatCpf() {
    this.cpf = Cpf.format(this.cpf.getValue());
  }

  @Override
  public void validate() {
    List<IAfterValidation<User>> validationHandlers = List.of(new FormatCpf());
    new UserValidator(new CpfValidator(), validationHandlers).validate(this);
  }
}
