package com.desafio.domain.user;

import com.desafio.shared.notification.INotification;

import java.math.BigDecimal;

public interface IUser {
  String getIdStr();
  String getCpfStr();
  BigDecimal getExchange();
  BigDecimal getAveragePrice();
  INotification getNotification();
}
