package com.desafio.domain.stock;

import com.desafio.domain.Identifier;

import java.util.Objects;
import java.util.UUID;

public class StockId extends Identifier {

  private final String value;

  public StockId(String value) {
    this.value = value;
  }

  public static StockId unique() {
    return StockId.from(UUID.randomUUID());
  }

  public static StockId from(String id) {
    return new StockId(id);
  }

  public static StockId from(UUID id) {
    return new StockId(id.toString().toLowerCase());
  }

  public String getValue() {
    return value;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    StockId stockId = (StockId) o;
    return getValue().equals(stockId.getValue());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getValue());
  }
}
