package com.desafio.domain.stock;

import com.desafio.domain.company.ICompany;
import com.desafio.domain.user.IUser;
import com.desafio.shared.notification.INotification;

import java.math.BigDecimal;
import java.time.Instant;

public interface IStock {
  String getIdStr();
  ICompany getCompany();
  String getTicker();
  IUser getUser();
  Integer getAmount();
  BigDecimal getPrice();
  BigDecimal getTotal();
  Instant getCreatedAt();
  Instant getUpdatedAt();
  Instant getDeletedAt();
  INotification getNotification();
  void sell(Integer amount);
  void buy(Integer amount);
}
