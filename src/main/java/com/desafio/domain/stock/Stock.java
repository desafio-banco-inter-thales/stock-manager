package com.desafio.domain.stock;

import com.desafio.domain.Aggregate;
import com.desafio.domain.company.ICompany;
import com.desafio.domain.user.IUser;
import com.desafio.shared.notification.INotification;
import com.desafio.shared.notification.Notification;

import java.math.BigDecimal;
import java.time.Instant;

public class Stock extends Aggregate<StockId> implements IStock {

  private IUser user;
  private ICompany company;
  private Integer amount;
  private BigDecimal price;
  private BigDecimal total;
  private Instant createdAt;
  private Instant updatedAt;
  private Instant deletedAt;

  protected Stock(String id, IUser user, ICompany company, Integer amount, BigDecimal price, BigDecimal total, Instant createdAt, Instant updatedAt, Instant deletedAt, INotification notification) {
    super(id == null? StockId.unique() : StockId.from(id), notification);
    this.user = user;
    this.company = company;
    this.amount = amount;
    this.price = price;
    this.total = total;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
    this.deletedAt = deletedAt;
  }

  public static Stock of(String id, IUser user, ICompany company, Integer amount, BigDecimal price, BigDecimal total) {
    Instant now = Instant.now();
    INotification notification = new Notification();
    return new Stock(id, user, company, amount, price, total, now, now, null, notification);
  }

  public static Stock of(String id, IUser user, ICompany company, Integer amount, BigDecimal price) {
    Instant now = Instant.now();
    INotification notification = new Notification();
    return new Stock(id, user, company, amount, price, null, now, now, null, notification);
  }

  public static Stock of(IUser user, ICompany company, Integer amount, BigDecimal price) {
    Instant now = Instant.now();
    INotification notification = new Notification();
    return new Stock(null, user, company, amount, price, null, now, now, null, notification);
  }

  public void buy(Integer amount) {
    this.updatedAt = Instant.now();
    this.amount += amount;
    calculateTotal();
  }

  public void sell(Integer amount) {
    this.updatedAt = Instant.now();
    this.amount -= amount;
    if(this.amount < 0)
      this.amount = 0;
    calculateTotal();
  }

  private void calculateTotal() {
    this.total = this.price.multiply(BigDecimal.valueOf(this.amount));
  }

  @Override
  public void validate() {
    new StockValidator().validate(this);
  }

  public IUser getUser() {
    return user;
  }

  @Override
  public String getIdStr() {
    return this.id.getValue();
  }

  public ICompany getCompany() {
    return company;
  }

  @Override
  public String getTicker() {
    return this.company.getTickerStr();
  }

  public Integer getAmount() {
    return amount;
  }

  public BigDecimal getPrice() {
    return price;
  }

  @Override
  public BigDecimal getTotal() {
    return total;
  }

  public Instant getCreatedAt() {
    return createdAt;
  }

  public Instant getUpdatedAt() {
    return updatedAt;
  }

  public Instant getDeletedAt() {
    return deletedAt;
  }
}
