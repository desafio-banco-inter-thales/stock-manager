package com.desafio.domain.stock;

import com.desafio.domain.Validator;

import java.math.BigDecimal;

import static com.desafio.shared.utils.Constants.STOCK_STR;
import static com.desafio.shared.utils.ErrorConstants.NEGATIVE_AMOUNT_OF_STOCKS;
import static com.desafio.shared.utils.ErrorConstants.NEGATIVE_PRICE_OF_STOCK;

public class StockValidator extends Validator<Stock> {

  @Override
  public void validate(Stock stock) {
    if(stock.getAmount() < 0)
      stock.getNotification().append(NEGATIVE_AMOUNT_OF_STOCKS, STOCK_STR);

    if(stock.getPrice().compareTo(BigDecimal.ZERO) < 0)
      stock.getNotification().append(NEGATIVE_PRICE_OF_STOCK, STOCK_STR);
  }

}
