CREATE SCHEMA IF NOT EXISTS desafio;

CREATE TABLE IF NOT EXISTS `company`(
    id VARCHAR(36) NOT NULL PRIMARY KEY,
    name VARCHAR(30) NOT NULL,
    ticker VARCHAR(7) NOT NULL,
    price DECIMAL(10, 2),
    is_active BIT DEFAULT 1
);

--INSERT INTO company (id, name, ticker, price, is_active) VALUES('1', 'Inter', 'BIDI11', 66.51, true);
--INSERT INTO company (id, name, ticker, price, is_active) VALUES('2', 'Magazine Luiza', 'MGLU3', 18.80, true);
--INSERT INTO company (id, name, ticker, price, is_active) VALUES('3', 'Sulamérica', 'SULA11', 28.26, false);
--INSERT INTO company (id, name, ticker, price, is_active) VALUES('4', 'Engie', 'EGIE3', 38.30, true);
--INSERT INTO company (id, name, ticker, price, is_active) VALUES('5', 'CVC', 'CVCB3', 20.87, true);
--INSERT INTO company (id, name, ticker, price, is_active) VALUES('6', 'Renner', 'LREN3', 36.95, true);
--INSERT INTO company (id, name, ticker, price, is_active) VALUES('7', 'Marisa', 'AMAR3', 6.3, true);
--INSERT INTO company (id, name, ticker, price, is_active) VALUES('8', 'Enjoei', 'ENJU3', 2.45, true);
--INSERT INTO company (id, name, ticker, price, is_active) VALUES('9', 'Banco do Brasil', 'BBSE3', 20.01, true);

CREATE TABLE IF NOT EXISTS `user`(
    id VARCHAR(36) NOT NULL PRIMARY KEY,
    cpf VARCHAR(11) NOT NULL,
    exchange DECIMAL(10, 2),
    average_price DECIMAL(10, 2)
);

--INSERT INTO `user` (id, cpf, exchange, average_price) VALUES('1', '05317350107', 11.2, 500);
--INSERT INTO `user` (id, cpf, exchange, average_price) VALUES('1', '05317350107', 0, 0);

CREATE TABLE IF NOT EXISTS `stock`(
    id VARCHAR(36) NOT NULL PRIMARY KEY,
    user_id VARCHAR(36) NOT NULL,
    company_id VARCHAR(36) NOT NULL,
    amount int NOT NULL,
    price DECIMAL(10, 2),
    created_at DATETIME(6),
    updated_at DATETIME(6),
    deleted_at DATETIME(6),
    FOREIGN KEY (company_id) REFERENCES company(id),
    FOREIGN KEY (user_id) REFERENCES `user`(id)
);

--INSERT INTO `stock` (id, user_id, company_id, amount, price, created_at, updated_at, deleted_at) VALUES('1', '1', '1', 14, 35.62, null, null, null);
--INSERT INTO `stock` (id, user_id, company_id, amount, price, created_at, updated_at, deleted_at) VALUES('3', '1', '2', 26, 18.80, null, null, null);