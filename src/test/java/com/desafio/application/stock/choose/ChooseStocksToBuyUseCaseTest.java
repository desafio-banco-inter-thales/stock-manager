package com.desafio.application.stock.choose;

import com.desafio.application.company.find.all.FindAllActiveCompaniesUseCase;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class ChooseStocksToBuyUseCaseTest {

  @InjectMocks
  private ChooseStocksToBuyUseCase chooseStocksToBuyUseCase;

  @Mock
  private FindAllActiveCompaniesUseCase findAllActiveCompaniesUseCase;

}
