package com.desafio.application.stock.buy;

import com.desafio.application.stock.StockDto;
import com.desafio.application.stock.GetAveragePriceUseCase;
import com.desafio.application.stock.SortListOfStocksUseCase;
import com.desafio.application.utils.TestUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static java.math.BigDecimal.valueOf;

public class BuyStocksOfUserUseCaseTest {

  GetAveragePriceUseCase getAveragePriceUseCase = GetAveragePriceUseCase.of();
  SortListOfStocksUseCase sortListOfStocksUseCase = SortListOfStocksUseCase.of(getAveragePriceUseCase);
  BuyStocksOfUserUseCase buyStocksOfUserUseCase = BuyStocksOfUserUseCase.of(getAveragePriceUseCase, sortListOfStocksUseCase);

  @Test
  void givenValidParams_whenCallBuyStocksOfUserUseCase_ShouldReturnValidOutput() {
    final var expectedOrder = valueOf(6000);
    final var expectedExchange = valueOf(14.94);
    final var expectedAveragePrice = valueOf(1197.01);
    final var expectedTotal = expectedOrder.subtract(expectedExchange);
    List<StockDto> stocks = TestUtils.createMockedStocks();
    List<StockDto> myStocks = sortListOfStocksUseCase.executeFromDto(stocks, BigDecimal.ZERO);
    final var output = buyStocksOfUserUseCase.execute(BuyUserStocksUseCaseCommand.of(expectedOrder, myStocks, BigDecimal.ZERO));

    final var total = output.stocks().stream().map(stock -> stock.price().multiply(valueOf(stock.amount()))).reduce(BigDecimal::add).get();
    Assertions.assertEquals(expectedExchange, output.exchange());
    Assertions.assertEquals(expectedAveragePrice, output.averagePrice());
    Assertions.assertEquals(expectedTotal, total);
  }

  @Test
  void givenInvalidNegativePreviousAveragePrice_whenCallSortListOfStocksUseCase_ShouldReturnNull() {
    final var expectedPreviousAveragePrice = valueOf(-2);
    List<StockDto> stocks = TestUtils.createMockedStocks();
    List<StockDto> myStocks = sortListOfStocksUseCase.executeFromDto(stocks, expectedPreviousAveragePrice);

    Assertions.assertNull(myStocks);
  }

  @Test
  void givenInvalidEmptyList_whenCallBuyStocksOfUserUseCase_ShouldReturnNull() {
    final var expectedOrder = valueOf(6000);
    List<StockDto> stocks = new ArrayList<>();
    final var output = buyStocksOfUserUseCase.execute(BuyUserStocksUseCaseCommand.of(expectedOrder, stocks, BigDecimal.ZERO));

    Assertions.assertNull(output);
  }

}
