package com.desafio.application.utils;

import com.desafio.application.company.CompanyDto;
import com.desafio.application.stock.StockDto;
import com.desafio.application.user.UserDto;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static com.desafio.shared.utils.Samples.*;

public class TestUtils {

  public static List<StockDto> createMockedStocks() {
    ArrayList<StockDto> stocks = new ArrayList<StockDto>();
    UserDto userDto = UserDto.of("123", CPF_SAMPLE, EXCHANGE_SAMPLE, AVERAGE_PRICE_SAMPLE);
    CompanyDto companyDto = CompanyDto.of("1", COMPANY_NAME_SAMPLE, TICKER_SAMPLE, AVERAGE_PRICE_SAMPLE, true);
    stocks.addAll(
            List.of(StockDto.of(userDto, companyDto, BigDecimal.valueOf(66.51), BigDecimal.ZERO),
                    StockDto.of(userDto, companyDto, BigDecimal.valueOf(18.80), BigDecimal.ZERO),
                    StockDto.of(userDto, companyDto, BigDecimal.valueOf(28.26), BigDecimal.ZERO),
                    StockDto.of(userDto, companyDto, BigDecimal.valueOf(38.30), BigDecimal.ZERO),
                    StockDto.of(userDto, companyDto, BigDecimal.valueOf(20.87), BigDecimal.ZERO)
            )
    );

    return stocks;
  }

  public static List<StockDto> createUnorderedButBalancedMockedStocks() {
    ArrayList<StockDto> stocks = new ArrayList<StockDto>();
    UserDto userDto = UserDto.of("123", CPF_SAMPLE, EXCHANGE_SAMPLE, AVERAGE_PRICE_SAMPLE);
    CompanyDto companyDto = CompanyDto.of("1", COMPANY_NAME_SAMPLE, TICKER_SAMPLE, AVERAGE_PRICE_SAMPLE, true);
    stocks.addAll(
            List.of(StockDto.of(userDto, companyDto, 26, BigDecimal.valueOf(66.51)),
                    StockDto.of(userDto, companyDto, 128, BigDecimal.valueOf(18.80)),
                    StockDto.of(userDto, companyDto, 84, BigDecimal.valueOf(28.26)),
                    StockDto.of(userDto, companyDto, 62, BigDecimal.valueOf(38.30)),
                    StockDto.of(userDto, companyDto, 116, BigDecimal.valueOf(20.87))
            )
    );

    return stocks;
  }

  public static List<StockDto> createUnorderedUnbalancedMockedStocks() {
    ArrayList<StockDto> stocks = new ArrayList<StockDto>();
    UserDto userDto = UserDto.of("123", CPF_SAMPLE, EXCHANGE_SAMPLE, AVERAGE_PRICE_SAMPLE);
    CompanyDto companyDto = CompanyDto.of("1", COMPANY_NAME_SAMPLE, TICKER_SAMPLE, AVERAGE_PRICE_SAMPLE, true);
    stocks.addAll(
            List.of(StockDto.of(userDto, companyDto, 26, BigDecimal.valueOf(66.51)),
                    StockDto.of(userDto, companyDto, 128, BigDecimal.valueOf(18.80)),
                    StockDto.of(userDto, companyDto, 0, BigDecimal.valueOf(28.26)),
                    StockDto.of(userDto, companyDto, 62, BigDecimal.valueOf(38.30)),
                    StockDto.of(userDto, companyDto, 0, BigDecimal.valueOf(20.87))
            )
    );

    return stocks;
  }

}
