package com.desafio.application.user.create;

import com.desafio.application.user.IUserGateway;
import com.desafio.application.user.find.FindUserUseCaseTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.desafio.shared.utils.Samples.CPF_SAMPLE;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class CreateUserUseCaseTest {

  @InjectMocks
  private CreateUserUseCase createUserUseCase;

  @Mock
  private IUserGateway userGateway;

  @Mock
  private FindUserUseCaseTest findUserUseCaseTest;

  @Test
  void givenValidCommand_whenCallsCreateUser_shouldCreateUser() {
    final var command = CreateUserCommand.of(CPF_SAMPLE);

    createUserUseCase.execute(command);

    verify(userGateway, times(1)).save(any());
  }
}
