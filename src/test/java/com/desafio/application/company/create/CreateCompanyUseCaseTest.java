package com.desafio.application.company.create;

import com.desafio.application.company.CompanyDto;
import com.desafio.application.company.ICompanyEntity;
import com.desafio.application.company.ICompanyGateway;
import com.desafio.application.company.find.output.FindCompanyByTickerOutput;
import com.desafio.application.company.find.ticker.FindCompanyByTickerUseCase;
import com.desafio.domain.company.Company;
import com.desafio.infrastructure.gateway.company.CompanyJpaEntity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Objects;

import static com.desafio.shared.utils.ErrorConstants.GATEWAY_INSERT_STR;
import static com.desafio.shared.utils.Samples.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class CreateCompanyUseCaseTest {

  @InjectMocks
  private CreateCompanyUseCase createCompanyUseCase;

  @Mock
  private FindCompanyByTickerUseCase findCompanyByTickerUseCase;

  @Mock
  private ICompanyGateway companyGateway;


  @Test
  void givenAValidCommand_whenCallsCreateCompany_shouldReturnInstance() {
    final var command =
            CreateCompanyCommand.of(COMPANY_NAME_SAMPLE, TICKER_SAMPLE, PRICE_SAMPLE, true);

    when(findCompanyByTickerUseCase.execute(any()))
            .thenReturn(FindCompanyByTickerOutput.of(TICKER_SAMPLE));


    final var actualOutput = createCompanyUseCase.execute(command);

    Assertions.assertNull(actualOutput.notification());
    Assertions.assertNotNull(actualOutput.id());

    verify(companyGateway, times(1)).save(argThat(company ->
            Objects.equals(COMPANY_NAME_SAMPLE, company.name())
                    && Objects.equals(TICKER_SAMPLE, company.ticker())
                    && Objects.equals(PRICE_SAMPLE, company.price())
                    && Objects.equals(true, company.isActive())
                    && Objects.nonNull(company.id())
    ));
  }

  @Test
  void givenAInvalidNullName_whenCallsCreateCompany_thenShouldReturnListOfErrors() {
    String expectedErrorMessage = "[COMPANY]: 'NAME' SHOULD NOT BE NULL.";
    final var command =
            CreateCompanyCommand.of(null, TICKER_SAMPLE, PRICE_SAMPLE, true);


    final var actualOutput = createCompanyUseCase.execute(command);

    Assertions.assertNotNull(actualOutput.notification());
    Assertions.assertNull(actualOutput.id());
    Assertions.assertEquals(1, actualOutput.notification().getErrors().size());
    Assertions.assertEquals(expectedErrorMessage, actualOutput.notification().messages(""));

    verify(companyGateway, times(0)).save(any());
  }

}
