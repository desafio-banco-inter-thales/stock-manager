package com.desafio.application.company.find;

import com.desafio.application.company.CompanyDto;
import com.desafio.application.company.ICompanyGateway;
import com.desafio.application.company.find.all.FindAllActiveCompaniesUseCase;
import com.desafio.domain.company.Company;
import com.desafio.infrastructure.gateway.company.CompanyJpaEntity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import static com.desafio.shared.utils.Samples.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class FindAllActiveCompaniesUseCaseTest {

  @Mock
  ICompanyGateway companyGateway;

  @InjectMocks
  FindAllActiveCompaniesUseCase findAllActiveCompaniesUseCase;

  @Test
  void givenValidParams_whenCallsFindAllActiveCompanies_thenShouldReturnCompanies() {
    final var companies = List.of(
            CompanyJpaEntity.of(CompanyDto.of(Company.of(COMPANY_NAME_SAMPLE, TICKER_SAMPLE, PRICE_SAMPLE, true))),
            CompanyJpaEntity.of(CompanyDto.of(Company.of("Google", "GOGL11", BigDecimal.valueOf(523.30), false)))
    );

    when(companyGateway.findAllActive()).thenReturn(companies.stream().filter(CompanyJpaEntity::isActive).collect(Collectors.toList()));

    final var output = findAllActiveCompaniesUseCase.execute();

    Assertions.assertEquals(1, output.companies().size());
  }

}
