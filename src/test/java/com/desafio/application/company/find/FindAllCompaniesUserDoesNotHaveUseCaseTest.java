package com.desafio.application.company.find;

import com.desafio.application.company.CompanyDto;
import com.desafio.application.company.ICompanyGateway;
import com.desafio.application.company.find.all.FindAllCompaniesUserAlreadyHasUseCase;
import com.desafio.application.company.find.all.FindAllCompaniesUserDoesNotHaveUseCase;
import com.desafio.domain.company.Company;
import com.desafio.infrastructure.gateway.company.CompanyJpaEntity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import static com.desafio.shared.utils.Samples.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class FindAllCompaniesUserDoesNotHaveUseCaseTest {

  @Mock
  ICompanyGateway companyGateway;

  @InjectMocks
  FindAllCompaniesUserDoesNotHaveUseCase findAllCompaniesUserDoesNotHaveUseCase;

  @Test
  void givenValidParams_whenCallsFindAllActiveCompanies_thenShouldReturnCompanies() {
    final var companies = List.of(
            CompanyJpaEntity.of(CompanyDto.of(Company.of(COMPANY_NAME_SAMPLE, TICKER_SAMPLE, PRICE_SAMPLE, true))),
            CompanyJpaEntity.of(CompanyDto.of(Company.of("Google", "GOGL11", BigDecimal.valueOf(523.30), false)))
    );

    List<CompanyJpaEntity> companiesUserAlreadyHave = companies.stream().filter(CompanyJpaEntity::isActive).toList();
    List<String> tickersUserAlreadyHave = companiesUserAlreadyHave.stream().map(CompanyJpaEntity::ticker).toList();

    when(companyGateway.findAllCompaniesUserDoesNotHave(tickersUserAlreadyHave)).thenReturn(companies.stream().filter(companyJpaEntity -> !companyJpaEntity.isActive()).collect(Collectors.toList()));

    final var output = findAllCompaniesUserDoesNotHaveUseCase.execute(tickersUserAlreadyHave);

    Assertions.assertEquals(1, output.companies().size());
    Assertions.assertEquals("GOGL11", output.companies().get(0).ticker());
  }

}
