package com.desafio.application.company.find;

import com.desafio.application.company.CompanyDto;
import com.desafio.application.company.ICompanyGateway;
import com.desafio.application.company.find.all.FindAllActiveCompaniesUseCase;
import com.desafio.application.company.find.all.FindAllCompaniesUserAlreadyHasUseCase;
import com.desafio.domain.company.Company;
import com.desafio.infrastructure.gateway.company.CompanyJpaEntity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import static com.desafio.shared.utils.Samples.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class FindAllCompaniesUserAlreadyHasUseCaseTest {

  @Mock
  ICompanyGateway companyGateway;

  @InjectMocks
  FindAllCompaniesUserAlreadyHasUseCase findAllCompaniesUserAlreadyHasUseCase;

  @Test
  void givenValidParams_whenCallsFindAllActiveCompanies_thenShouldReturnCompanies() {
    final var companies = List.of(
            CompanyJpaEntity.of(CompanyDto.of(Company.of(COMPANY_NAME_SAMPLE, TICKER_SAMPLE, PRICE_SAMPLE, true))),
            CompanyJpaEntity.of(CompanyDto.of(Company.of("Google", "GOGL11", BigDecimal.valueOf(523.30), false)))
    );

    List<CompanyJpaEntity> companiesUserAlreadyHave = companies.stream().filter(CompanyJpaEntity::isActive).toList();
    List<String> tickersUserAlreadyHave = companiesUserAlreadyHave.stream().map(CompanyJpaEntity::ticker).toList();

    when(companyGateway.findAllCompaniesUserAlreadyHas(tickersUserAlreadyHave)).thenReturn(companies.stream().filter(CompanyJpaEntity::isActive).collect(Collectors.toList()));

    final var output = findAllCompaniesUserAlreadyHasUseCase.execute(tickersUserAlreadyHave);

    Assertions.assertEquals(1, output.companies().size());
    Assertions.assertEquals(TICKER_SAMPLE, output.companies().get(0).ticker());
  }

}
