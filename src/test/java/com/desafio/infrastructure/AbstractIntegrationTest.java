package com.desafio.infrastructure;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.containers.KafkaContainer;
import org.testcontainers.utility.DockerImageName;

@SpringBootTest
public abstract class AbstractIntegrationTest {

  static KafkaContainer kafka =
      new KafkaContainer(DockerImageName.parse("confluentinc/cp-kafka:6.2.1"));


  static {
    kafka.start();

  }

  @DynamicPropertySource
  public static void overrideProps(DynamicPropertyRegistry registry) {
    registry.add("spring.cloud.stream.kafka.binder.brokers", () -> kafka.getHost());
    registry.add("spring.cloud.stream.kafka.binder.brokers", () -> kafka.getHost());
    registry.add(
        "spring.cloud.stream.kafka.binder.defaultBrokerPort", () -> kafka.getFirstMappedPort());
  }
}
