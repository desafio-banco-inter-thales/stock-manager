package com.desafio.infrastructure.stream;

import com.desafio.application.company.CompanyDto;
import com.desafio.application.user.IUserGateway;
import com.desafio.infrastructure.IntegrationTest;
import com.desafio.infrastructure.configuration.KafkaProducerConfiguration;
import com.desafio.infrastructure.gateway.company.CompanyJpaEntity;
import com.desafio.infrastructure.gateway.company.CompanyRepository;
import com.desafio.infrastructure.gateway.company.CompanySqlGateway;
import com.desafio.infrastructure.gateway.user.UserRepository;
import com.desafio.infrastructure.stream.events.CompanyEvent;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.kafka.core.KafkaTemplate;

import java.math.BigDecimal;
import java.time.Duration;
import java.util.Optional;

import static com.desafio.shared.utils.Samples.CPF_SAMPLE;
import static org.testcontainers.shaded.org.awaitility.Awaitility.await;

@IntegrationTest
public class EventConsumerIT extends KafkaProducerConfiguration {

  @Autowired
  private KafkaTemplate<String, CompanyEvent> eventProducer;

  @Autowired
  protected CompanyRepository repository;

  @BeforeEach
  void setup() {
    repository.deleteAll();
  }

  @AfterEach
  void shutdown() {
    repository.deleteAll();
  }

  @Test
  void whenCompanyEventOfCreateReceived_ShouldAddCompanyToDatabase() {

    CompanyEvent inputEvent = CompanyEvent.of("save", "Fundo Hectare", "HCTR11", BigDecimal.valueOf(110), true, CPF_SAMPLE);
    eventProducer.send("COMPANY_EVENT", inputEvent);

    final var createdCompany = repository.findByTicker("HCTR11");

    await().atMost(Duration.ofSeconds(5)).untilAsserted(() -> Assertions.assertNotNull(createdCompany));
  }

}
