package com.desafio.domain.user;

import com.desafio.domain.user.cpf.Cpf;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static com.desafio.shared.utils.ErrorConstants.EXCHANGE_MUST_NOT_BE_NEGATIVE;
import static com.desafio.shared.utils.Samples.CPF_SAMPLE;

public class UserTest {

  @Test
  void givenValidParamsWithCpfWithinPunctuation_whenCallNewUser_thenShouldCreateUserWithCpfWithinPunctuation() {
    final User createdUser = User.of(CPF_SAMPLE, BigDecimal.ZERO, BigDecimal.ZERO);
    createdUser.validate();

    Assertions.assertFalse(createdUser.getNotification().hasErrors());
    Assertions.assertNotNull(createdUser);
    Assertions.assertNotNull(createdUser.getId());
    Assertions.assertEquals(Cpf.of(CPF_SAMPLE), createdUser.getCpf());
  }

  @Test
  void givenInvalidNegativeExchange_whenCallNewUser_thenReturnListOfErrors() {
    final Integer expected_error_count = 1;

    final User createdUser = User.of(CPF_SAMPLE, BigDecimal.valueOf(-1), BigDecimal.ZERO);
    createdUser.validate();

    Assertions.assertEquals(expected_error_count, createdUser.getNotification().getErrors().size());
    Assertions.assertEquals(EXCHANGE_MUST_NOT_BE_NEGATIVE, createdUser.getNotification().getErrors().get(0).message());
  }

}
