package com.desafio.domain.company;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static com.desafio.shared.utils.Constants.NAME_STR;
import static com.desafio.shared.utils.ErrorConstants.STRING_SHOULD_NOT_BE_NULL;
import static com.desafio.shared.utils.Samples.*;

public class CompanyTest {

  @Test
  void givenValidParams_whenCallCreateCompany_thenShouldCreateCompany() {
    final Company createdCompany = Company.of(COMPANY_NAME_SAMPLE, TICKER_SAMPLE, PRICE_SAMPLE, true);
    createdCompany.validate();

    Assertions.assertNotNull(createdCompany);
    Assertions.assertNotNull(createdCompany.getId());
    Assertions.assertTrue(createdCompany.getActive());
    Assertions.assertEquals(COMPANY_NAME_SAMPLE, createdCompany.getName());
    Assertions.assertEquals(TICKER_SAMPLE, createdCompany.getTicker().getValue());
    Assertions.assertEquals(PRICE_SAMPLE, createdCompany.getTicker().getPrice());
  }

  @Test
  void givenInvalidNullName_whenCallCreateCompany_thenReturnListOfErrors() {
    final Integer expected_error_count = 1;
    final String expected_error_message = STRING_SHOULD_NOT_BE_NULL.replace("{}", NAME_STR);

    final Company company = Company.of(null, TICKER_SAMPLE, PRICE_SAMPLE, false);
    company.validate();

    Assertions.assertEquals(expected_error_count, company.getNotification().getErrors().size());
    Assertions.assertEquals(expected_error_message, company.getNotification().getErrors().get(0).message());
  }

}
