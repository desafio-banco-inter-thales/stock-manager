package com.desafio.domain.stock;

import com.desafio.domain.company.Company;
import com.desafio.domain.user.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static com.desafio.shared.utils.ErrorConstants.NEGATIVE_AMOUNT_OF_STOCKS;
import static com.desafio.shared.utils.Samples.*;
import static com.desafio.shared.utils.Samples.PRICE_SAMPLE;

public class StockTest {

  @Test
  void givenValidParams_whenCallCreateStock_thenShouldCreateStock() {
    final User createdUser = User.of(CPF_SAMPLE, BigDecimal.ZERO, BigDecimal.ZERO);
    final Company createdCompany = Company.of(COMPANY_NAME_SAMPLE, TICKER_SAMPLE, PRICE_SAMPLE, true);
    final Stock createdStock = Stock.of(createdUser, createdCompany, AMOUNT_SAMPLE, PRICE_SAMPLE);
    createdStock.validate();

    Assertions.assertNotNull(createdStock);
    Assertions.assertNotNull(createdStock.getId());
    Assertions.assertNotNull(createdStock.getUser());
    Assertions.assertNotNull(createdStock.getCompany());
    Assertions.assertNotNull(createdStock.getCreatedAt());
    Assertions.assertNotNull(createdStock.getUpdatedAt());
    Assertions.assertNull(createdStock.getDeletedAt());
    Assertions.assertEquals(AMOUNT_SAMPLE, createdStock.getAmount());
    Assertions.assertEquals(PRICE_SAMPLE, createdStock.getPrice());
  }

  @Test
  void givenValidStock_whenCallBuyStocks_thenShouldIncreaseStocks() {
    final User createdUser = User.of(CPF_SAMPLE, BigDecimal.ZERO, BigDecimal.ZERO);
    final Company createdCompany = Company.of(COMPANY_NAME_SAMPLE, TICKER_SAMPLE, PRICE_SAMPLE, true);
    final Stock createdStock = Stock.of(createdUser, createdCompany, AMOUNT_SAMPLE, PRICE_SAMPLE);
    createdStock.validate();

    createdStock.buy(AMOUNT_SAMPLE);

    Assertions.assertTrue(createdStock.getAmount() > AMOUNT_SAMPLE);
    Assertions.assertEquals(AMOUNT_SAMPLE+AMOUNT_SAMPLE, createdStock.getAmount());
  }

  @Test
  void givenValidStock_whenCallSellStocks_thenShouldDecreaseStocks() {
    final User createdUser = User.of(CPF_SAMPLE, BigDecimal.ZERO, BigDecimal.ZERO);
    final Company createdCompany = Company.of(COMPANY_NAME_SAMPLE, TICKER_SAMPLE, PRICE_SAMPLE, true);
    final Stock createdStock = Stock.of(createdUser, createdCompany, AMOUNT_SAMPLE, PRICE_SAMPLE);
    createdStock.validate();

    createdStock.sell(AMOUNT_SAMPLE);

    Assertions.assertTrue(createdStock.getAmount() < AMOUNT_SAMPLE);
    Assertions.assertEquals(0, createdStock.getAmount());
  }

  @Test
  void givenValidStock_whenCallSellMoreStocksThanUserHas_thenShouldDecreaseStocksToZero() {
    final User createdUser = User.of(CPF_SAMPLE, BigDecimal.ZERO, BigDecimal.ZERO);
    final Company createdCompany = Company.of(COMPANY_NAME_SAMPLE, TICKER_SAMPLE, PRICE_SAMPLE, true);
    final Stock createdStock = Stock.of(createdUser, createdCompany, 0, PRICE_SAMPLE);
    createdStock.validate();

    createdStock.sell(AMOUNT_SAMPLE);

    Assertions.assertEquals(0, createdStock.getAmount());
  }

  @Test
  void givenInvalidStock_whenCallValidateStock_thenReturnListOfErrors() {
    final var expected_error_count = 1;
    final User createdUser = User.of(CPF_SAMPLE, BigDecimal.ZERO, BigDecimal.ZERO);
    final Company createdCompany = Company.of(COMPANY_NAME_SAMPLE, TICKER_SAMPLE, PRICE_SAMPLE, true);
    final Stock createdStock = Stock.of(createdUser, createdCompany, -20, PRICE_SAMPLE);
    createdStock.validate();

    Assertions.assertEquals(expected_error_count, createdStock.getNotification().getErrors().size());
    Assertions.assertEquals(NEGATIVE_AMOUNT_OF_STOCKS, createdStock.getNotification().getErrors().get(0).message());
  }

}
